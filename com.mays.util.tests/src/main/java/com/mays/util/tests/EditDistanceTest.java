/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.tests;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.mays.util.spell.Spell;
import com.mays.util.spell.Spell.Alignment;
import com.mays.util.spell.Spell.AlignmentList;

public class EditDistanceTest {

	@Test
	public void testIns1() {
		Assert.assertTrue(Spell.editDistance("abcdef", "abcdefg") == 1);
	}

	@Test
	public void testAlignIns1() throws Exception {
		Alignment res = Spell.editDistanceAlign("abcdef", "abcdefg");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, "abcdef ");
		Assert.assertEquals(res.a2, "abcdefg");
	}

	@Test
	public void testIns2() {
		Assert.assertTrue(Spell.editDistance("bcdefg", "abcdefg") == 1);
	}

	@Test
	public void testAlignIns2() throws Exception {
		Alignment res = Spell.editDistanceAlign("bcdefg", "abcdefg");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, " bcdefg");
		Assert.assertEquals(res.a2, "abcdefg");
	}

	@Test
	public void testIns3() {
		Assert.assertTrue(Spell.editDistance("aceg", "abcdefg") == 3);
	}

	@Test
	public void testAlignIns3() throws Exception {
		Alignment res = Spell.editDistanceAlign("aceg", "abcdefg");
		Assert.assertTrue(res.distance == 3);
		Assert.assertEquals(res.a1, "a c e g");
		Assert.assertEquals(res.a2, "abcdefg");
	}

	@Test
	public void testDel1() {
		Assert.assertTrue(Spell.editDistance("abcdefg", "abcdef") == 1);
	}

	@Test
	public void testAlignDel1() throws Exception {
		Alignment res = Spell.editDistanceAlign("abcdefg", "abcdef");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, "abcdefg");
		Assert.assertEquals(res.a2, "abcdef ");
	}

	@Test
	public void testDel2() {
		Assert.assertTrue(Spell.editDistance("bcdefg", "abcdefg") == 1);
	}

	@Test
	public void testAlignDel2() throws Exception {
		Alignment res = Spell.editDistanceAlign("bcdefg", "abcdefg");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, " bcdefg");
		Assert.assertEquals(res.a2, "abcdefg");
	}

	@Test
	public void testDel3() {
		Assert.assertTrue(Spell.editDistance("abcdefg", "aceg") == 3);
	}

	@Test
	public void testAlignDel3() throws Exception {
		Alignment res = Spell.editDistanceAlign("abcdefg", "aceg");
		Assert.assertTrue(res.distance == 3);
		Assert.assertEquals(res.a1, "abcdefg");
		Assert.assertEquals(res.a2, "a c e g");
	}

	@Test
	public void testSub1() {
		Assert.assertTrue(Spell.editDistance("abcdefg", "abcxefg") == 1);
	}

	@Test
	public void testAlignSub1() throws Exception {
		Alignment res = Spell.editDistanceAlign("abcdefg", "abcxefg");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, "abcdefg");
		Assert.assertEquals(res.a2, "abcxefg");
	}

	@Test
	public void testSub2() {
		Assert.assertTrue(Spell.editDistance("abcxefg", "abcdefg") == 1);
	}

	@Test
	public void testAlignSub2() throws Exception {
		Alignment res = Spell.editDistanceAlign("abcxefg", "abcdefg");
		Assert.assertTrue(res.distance == 1);
		Assert.assertEquals(res.a1, "abcxefg");
		Assert.assertEquals(res.a2, "abcdefg");
	}

	@Test
	public void testInsW1() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "b", "c", "d", "e", "f" },
				new String[] { "a", "b", "c", "d", "e", "f", "g" }) == 1);
	}

	@Test
	public void testAlignInsW1() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 1);
			ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
			a1.add("*");
			Assert.assertTrue(res.a1.equals(a1));
			Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
		}
	}

	@Test
	public void testInsW2() {
		Assert.assertTrue(Spell.editDistance(new String[] { "b", "c", "d", "e", "f", "g" },
				new String[] { "a", "b", "c", "d", "e", "f", "g" }) == 1);
	}

	@Test
	public void testAlignInsW2() throws Exception {
		String[] w1 = new String[] { "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
			a1.add(0, "*");
			Assert.assertTrue(res.a1.equals(a1));
			Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
		}
	}

	@Test
	public void testInsW3() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "c", "e", "g" },
				new String[] { "a", "b", "c", "d", "e", "f", "g" }) == 3);
	}

	@Test
	public void testAlignInsW3() throws Exception {
		String[] w1 = new String[] { "a", "c", "e", "g" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 3);
			ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
			a1.add(1, "*");
			a1.add(3, "*");
			a1.add(5, "*");
			Assert.assertTrue(res.a1.equals(a1));
			Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
		}
	}

	@Test
	public void testInsW4() {
		Assert.assertTrue(
				Spell.editDistance(new String[] { "a", "b", "c" }, new String[] { "a", "b", "c", "c", "c", "c" }) == 3);
	}

	@Test
	public void testAlignInsW4() throws Exception {
		String[] w1 = new String[] { "a", "b", "c" };
		String[] w2 = new String[] { "a", "b", "c", "c", "c", "c" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 3);
			ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
			a1.add(2, "*");
			a1.add(2, "*");
			a1.add(2, "*");
			Assert.assertTrue(res.a1.equals(a1));
			Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
		}
	}

	@Test
	public void testDelW1() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "b", "c", "d", "e", "f", "g" },
				new String[] { "a", "b", "c", "d", "e", "f" }) == 1);
	}

	@Test
	public void testAlignDelW1() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 1);
			Assert.assertTrue(res.a1.equals(Arrays.asList(w1)));
			ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w2));
			a2.add("*");
			Assert.assertTrue(res.a2.equals(a2));
		}
	}

	@Test
	public void testDelW2() {
		Assert.assertTrue(Spell.editDistance(new String[] { "b", "c", "d", "e", "f", "g" },
				new String[] { "a", "b", "c", "d", "e", "f", "g" }) == 1);
	}

	@Test
	public void testAlignDelW2() throws Exception {
		String[] w1 = new String[] { "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 1);
			ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
			a1.add(0, "*");
			Assert.assertTrue(res.a1.equals(a1));
			Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
		}
	}

	@Test
	public void testDelW3() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "b", "c", "d", "e", "f", "g" },
				new String[] { "a", "c", "e", "g" }) == 3);
	}

	@Test
	public void testAlignDelW3() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "c", "e", "g" };
		for (boolean mcs : Arrays.asList(true, false)) {
			AlignmentList res = Spell.editDistanceAlign(w1, w2, mcs);
			Assert.assertTrue(res.distance == 3);
			Assert.assertTrue(res.a1.equals(Arrays.asList(w1)));
			ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w2));
			a2.add(1, "*");
			a2.add(3, "*");
			a2.add(5, "*");
			Assert.assertTrue(res.a2.equals(a2));
		}
	}

	@Test
	public void testSubW1() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "b", "c", "d", "e", "f", "g" },
				new String[] { "a", "b", "c", "x", "e", "f", "g" }) == 1);
	}

	@Test
	public void testAlignSubW1() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "x", "e", "f", "g" };
		AlignmentList res = Spell.editDistanceAlign(w1, w2, true);
		Assert.assertTrue(res.distance == 2);
		Assert.assertTrue(res.a1.subList(0, 3).equals(Arrays.asList(w1).subList(0, 3)));
		Assert.assertTrue(res.a1.subList(5, 8).equals(Arrays.asList(w1).subList(4, 7)));
		Assert.assertTrue(res.a2.subList(0, 3).equals(Arrays.asList(w2).subList(0, 3)));
		Assert.assertTrue(res.a2.subList(5, 8).equals(Arrays.asList(w2).subList(4, 7)));
		Assert.assertTrue(res.a1.subList(0, 3).equals(res.a2.subList(0, 3)));
		Assert.assertTrue(res.a1.subList(5, 8).equals(res.a2.subList(5, 8)));
	}

	@Test
	public void testAlignSubW1A() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "x", "e", "f", "g" };
		AlignmentList res = Spell.editDistanceAlign(w1, w2, false);
		Assert.assertTrue(res.distance == 1);
		Assert.assertTrue(res.a1.equals(Arrays.asList(w1)));
		Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));

	}

	@Test
	public void testSubW2() {
		Assert.assertTrue(Spell.editDistance(new String[] { "a", "b", "c", "x", "e", "f", "g" },
				new String[] { "a", "b", "c", "d", "e", "f", "g" }) == 1);
	}

	@Test
	public void testAlignSubW2() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "x", "e", "f", "g" };
		AlignmentList res = Spell.editDistanceAlign(w1, w2, true);
		Assert.assertTrue(res.distance == 2);
		Assert.assertTrue(res.a1.subList(0, 3).equals(Arrays.asList(w1).subList(0, 3)));
		Assert.assertTrue(res.a1.subList(5, 8).equals(Arrays.asList(w1).subList(4, 7)));
		Assert.assertTrue(res.a2.subList(0, 3).equals(Arrays.asList(w2).subList(0, 3)));
		Assert.assertTrue(res.a2.subList(5, 8).equals(Arrays.asList(w2).subList(4, 7)));
		Assert.assertTrue(res.a1.subList(0, 3).equals(res.a2.subList(0, 3)));
		Assert.assertTrue(res.a1.subList(5, 8).equals(res.a2.subList(5, 8)));
	}

	@Test
	public void testAlignSubW2A() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		String[] w2 = new String[] { "a", "b", "c", "x", "e", "f", "g" };
		AlignmentList res = Spell.editDistanceAlign(w1, w2, false);
		Assert.assertTrue(res.distance == 1);
		Assert.assertTrue(res.a1.equals(Arrays.asList(w1)));
		Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));

	}

	// TODO revisit edit distance words
	@Test
	public void testAlignWords1() throws Exception {
		String[] w1 = new String[] { "a", "b", "cat", "dog", "e", "f" };
		String[] w2 = new String[] { "a", "b", "fog", "e", "f" };
		AlignmentList res = Spell.editDistanceAlign(w1, w2, false);
		Assert.assertTrue(res.distance == 2);
		Assert.assertTrue(res.a1.equals(Arrays.asList(w1)));
		ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w2));
		a2.add(2, "*");
		Assert.assertTrue(res.a2.equals(a2));
	}

}
