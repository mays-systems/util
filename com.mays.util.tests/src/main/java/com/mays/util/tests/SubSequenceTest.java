/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.tests;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.mays.util.spell.Spell;
import com.mays.util.spell.Spell.AlignmentList;

public class SubSequenceTest {

	@Test
	public void testAlignInsW1() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		AlignmentList res = Spell.longestCommonSubSequenceAlign(w1, w2);
		Assert.assertTrue(res.distance == 6);
		ArrayList<String> a1 = new ArrayList<>(Arrays.asList(w1));
		a1.add("*");
		Assert.assertTrue(res.a1.equals(a1));
		Assert.assertTrue(res.a2.equals(Arrays.asList(w2)));
	}

	@Test
	public void testAlignInsW1Flip() throws Exception {
		String[] w1 = new String[] { "a", "b", "c", "d", "e", "f" };
		String[] w2 = new String[] { "a", "b", "c", "d", "e", "f", "g" };
		AlignmentList res = Spell.longestCommonSubSequenceAlign(w2, w1);
		Assert.assertTrue(res.distance == 6);
		Assert.assertTrue(res.a1.equals(Arrays.asList(w2)));
		ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w1));
		a2.add("*");
		Assert.assertTrue(res.a2.equals(a2));
	}

	@Test
	public void testAlignSpace() throws Exception {
		String[] w1 = new String[] { "a", " ", "d", " ", "F" };
		String[] w2 = new String[] { "a", "e", " ", "F", };
		AlignmentList res = Spell.longestCommonSubSequenceAlign(w2, w1);
		Assert.assertTrue(res.distance == 3);
		// Assert.assertTrue(res.a1.equals(Arrays.asList(w2)));
		// ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w1));
		// a2.add("*");
		// Assert.assertTrue(res.a2.equals(a2));
	}

	@Test
	public void testAlignX() throws Exception {
		String[] w1 = new String[] { "a", "d", "e", "F", "i", "k", "m" };
		String[] w2 = new String[] { "a", "e", "F", "g", "h", "F", "j", "l", "m", "n", "o" };
		AlignmentList res = Spell.longestCommonSubSequenceAlign(w2, w1);
		Assert.assertTrue(res.distance == 4);
		// Assert.assertTrue(res.a1.equals(Arrays.asList(w2)));
		// ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w1));
		// a2.add("*");
		// Assert.assertTrue(res.a2.equals(a2));
	}

	@Test
	public void testAlignSingle() throws Exception {
		String[] w1 = new String[] { "a" };
		String[] w2 = new String[] { "d" };
		AlignmentList res = Spell.longestCommonSubSequenceAlign(w2, w1);
		Assert.assertTrue(res.distance == 0);
		// Assert.assertTrue(res.a1.equals(Arrays.asList(w2)));
		// ArrayList<String> a2 = new ArrayList<>(Arrays.asList(w1));
		// a2.add("*");
		// Assert.assertTrue(res.a2.equals(a2));
	}

}
