/**
 * Copyright 2012-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.poi;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PoiUtil {

	public static class LengthFormula {

		public String getText(Cell cell) {
			return "LEN(" + ((char) ('A' + cell.getColumnIndex() + offset)) + (cell.getRowIndex() + 1) + ")";
		}

		int offset;

		public LengthFormula(int offset) {
			super();
			this.offset = offset;
		}

	}

	public static String readCell(Row row, char index, Logger logger) throws Exception {
		return readCell(row, index - 'A', logger);
	}

	public static String readCell(Row row, int index, Logger logger) throws Exception {
		return readCell(row.getCell(index, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL), logger);
	}

	public static String readCell(Cell cell, Logger logger) throws Exception {
		if (cell == null)
			return null;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_NUMERIC:
			return String.valueOf((int) cell.getNumericCellValue());
		case Cell.CELL_TYPE_STRING: {
			String str = cell.getStringCellValue();
			str = str.replace((char) 160, ' '); // non breaking space
			return str.trim();
		}
		case Cell.CELL_TYPE_BLANK:
			return cell.getStringCellValue().trim();
		case Cell.CELL_TYPE_BOOLEAN:
			return String.valueOf(cell.getBooleanCellValue());
		}
		String msg = "Bad cell type: " + cell.getCellType() + " " + cell.getRowIndex() + " " + cell.getColumnIndex();
		if (logger != null) {
			logger.severe(msg);
		} else {
			throw new Exception(msg);
		}
		return null;
	}

	public static void copyCell(Cell fr, Cell to, Logger logger) throws Exception {
		if (fr == null)
			return;
		switch (fr.getCellType()) {
		case 0: {
			to.setCellValue(fr.getNumericCellValue());
			return;
		}
		case 1:
			to.setCellValue(fr.getStringCellValue());
			return;
		case 3:
			to.setCellValue(fr.getStringCellValue());
			return;
		}
		String msg = "Bad cell type: " + fr.getCellType() + " " + fr.getRowIndex() + " " + fr.getColumnIndex();
		if (logger != null) {
			logger.severe(msg);
		} else {
			throw new Exception(msg);
		}
	}

	public static void copyRow(Row fr, Row to, Logger logger) throws Exception {
		PoiUtil.copyCells(fr, 0, fr.getLastCellNum() - 1, to, logger);
	}

	public static void copyCells(Row fr, char beg_i, char end_i, Row to, Logger logger) throws Exception {
		copyCells(fr, beg_i - 'A', end_i - 'A', to, logger);
	}

	public static void copyCells(Row fr, int beg_i, int end_i, Row to, Logger logger) throws Exception {
		for (int i = beg_i; i <= end_i; i++) {
			PoiUtil.copyCell(fr.getCell(i), to.createCell(i), logger);
		}
	}

	public static void copyColumnWidth(Sheet fr, Sheet to, int col_i) throws Exception {
		for (int i = 0; i <= col_i; i++) {
			to.setColumnWidth(i, fr.getColumnWidth(i));
		}
	}

	public static void setColumnWidth(Sheet sheet, int index, int... value) throws Exception {
		for (int s : value) {
			sheet.setColumnWidth(index, s * 256);
			index++;
		}
	}

	@Deprecated
	public static void setCellValueInt(Row row, int index, int value) throws Exception {
		Cell cell = row.createCell(index);
		cell.setCellValue(value);
	}

	public static void setCellValue(Row row, int index, int value) throws Exception {
		Cell cell = row.createCell(index);
		cell.setCellValue(value);
	}

	public static void setCellValue(Row row, char index, String... value) throws Exception {
		setCellValue(row, index - 'A', value);
	}

	public static void setCellValue(Row row, int index, String... value) throws Exception {
		for (String s : value) {
			Cell cell = row.createCell(index);
			cell.setCellValue(s);
			index++;
		}
	}

	public static Row createHeader(Sheet sheet, List<String> headings) throws Exception {
		return createHeader(sheet, headings.toArray(new String[headings.size()]));
	}

	public static Row createHeader(Sheet sheet, String... headings) throws Exception {
		Row row = createRow(sheet, 0, headings);
		sheet.createFreezePane(0, 1);
		return row;
	}

	public static Row createRow(Sheet sheet, List<String> headings) throws Exception {
		return createRow(sheet, headings.toArray(new String[headings.size()]));
	}

	public static Row createRow(Sheet sheet, String... headings) throws Exception {
		return createRow(sheet, sheet.getLastRowNum() + 1, headings);
	}

	public static Row createRow(Sheet sheet, int row_i, List<String> headings) throws Exception {
		return createRow(sheet, row_i, headings.toArray(new String[headings.size()]));
	}

	public static Row createRow(Sheet sheet, int row_i, String... headings) throws Exception {
		Row row = sheet.createRow(row_i);
		int i = 0;
		for (String s : headings) {
			Cell cell = row.createCell(i);
			cell.setCellValue(s);
			i++;
		}
		return row;
	}

	public static void appendToRow(Row row, String... headings) throws Exception {
		for (String s : headings) {
			row.createCell(row.getLastCellNum()).setCellValue(s);
		}
	}

	public static String xlsFile(String file) throws Exception {
		return xlsFile(file, 1);
	}

	public static String xlsFile(String file, int freeze_row) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet();
		String line = "";
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			String[] fields = line.split("\\t");
			// if (line_i == 1) {
			// PoiUtil.createHeader(sheet, Arrays.asList(fields));
			// continue;
			// }
			Row row = sheet.createRow((line_i == 1 ? 0 : sheet.getLastRowNum() + 1));
			for (int i = 0; i < fields.length; i++) {
				row.createCell(i).setCellValue(fields[i]);
			}
		}
		sheet.createFreezePane(0, freeze_row);
		String xls = file.replaceFirst(".txt$", ".xlsx");
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(xls));
		wb.write(out);
		out.close();
		wb.close();
		in.close();
		return xls;
	}

}
