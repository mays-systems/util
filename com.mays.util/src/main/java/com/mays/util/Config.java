/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Simple configuration management with logging
 * 
 * @author Eric Mays
 * 
 */
public class Config extends Properties {

	private static final long serialVersionUID = 1L;

	public static String getConfigName(String[] args, Logger logger)
			throws Exception {
		return getConfigName(args, 0, logger);
	}

	public static String getConfigName(String[] args, int arg_i,
			Logger logger) throws Exception {
		String config_name = null;
		if (args.length > arg_i)
			config_name = args[arg_i];
		if (logger != null)
			logger.config("Config name: " + config_name);
		return config_name;
	}

	public static Config load(String config_name, Logger logger)
			throws Exception {
		if (logger != null)
			logger.config("Loading config from: " + config_name);
		return load(config_name);
	}

	public static Config load(String config_name) throws Exception {
		Config config = new Config();
		// this is for 1.6 - mac on 1.5
		// config.load(new BufferedReader(new FileReader(config_name)));
		config.load(new FileInputStream(config_name));
		return config;
	}

	public String getProperty(String key, Logger logger) {
		String value = this.getProperty(key);
		if (logger != null)
			logger.config(key + ": " + value);
		return value;
	}

}
