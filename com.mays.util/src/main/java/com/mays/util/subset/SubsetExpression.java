/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.util.UUID;

/**
 * @author emays
 * 
 */
public class SubsetExpression {

	private Operator operator;

	private Modifier modifier;

	private boolean concept_p;

	private long concept_id;

	private UUID subset_id;

	@Deprecated
	public SubsetExpression(Operator operator, Modifier modifier, boolean concept_p, long concept_id) {
		this(operator, modifier, concept_id);
		assert concept_p;
	}

	public SubsetExpression(long concept_id) {
		this(Operator.INCLUDE, Modifier.DESCENDANTS, concept_id);
	}

	public SubsetExpression(Operator operator, Modifier modifier, long concept_id) {
		this.operator = operator;
		this.modifier = modifier;
		this.concept_p = true;
		this.concept_id = concept_id;
		this.subset_id = null;
	}

	public SubsetExpression(Operator operator, UUID subset_id) {
		this.operator = operator;
		this.modifier = Modifier.ONLY;
		this.concept_p = false;
		this.concept_id = -1;
		this.subset_id = subset_id;
	}

	public long getConceptId() {
		return concept_id;
	}

	public void setConceptId(long concept_id) {
		this.concept_id = concept_id;
	}

	public UUID getSubsetId() {
		return this.subset_id;
	}

	public void setSubsetId(UUID subset_id) {
		this.subset_id = subset_id;
	}

	public boolean isConcept() {
		return concept_p;
	}

	public void setConceptP(boolean concept_p) {
		this.concept_p = concept_p;
	}

	public Modifier getModifier() {
		return modifier;
	}

	public void setModifier(Modifier modifier) {
		this.modifier = modifier;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SubsetExpression))
			return super.equals(obj);
		SubsetExpression o2 = (SubsetExpression) obj;
		return (this.operator == o2.operator && this.modifier == o2.modifier && this.concept_p == o2.concept_p
				&& this.concept_id == o2.concept_id && this.subset_id == o2.subset_id);
	}

}
