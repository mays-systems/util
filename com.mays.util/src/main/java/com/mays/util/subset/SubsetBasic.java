/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class SubsetBasic implements Comparable<SubsetBasic> {

	private UUID id;

	private String name;

	private long last_modified;

	private boolean built_p;

	public SubsetBasic(UUID id, String name, long last_modified, boolean built_p) {
		super();
		this.id = id;
		this.name = name;
		this.last_modified = last_modified;
		this.built_p = built_p;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastModified() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
		return formatter.format(new Date(this.last_modified));
	}

	public boolean isBuilt() {
		return this.built_p;
	}

	public int compareTo(SubsetBasic o) {
		return this.getName().compareTo(o.getName());
	}

	public String toString() {
		return this.getName();
	}

}
