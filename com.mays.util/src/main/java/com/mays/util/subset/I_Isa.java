/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.util.ArrayList;

/**
 * 
 * @author Eric Mays
 */
public interface I_Isa {

	public String getLogString(long con) throws Exception;

	public ArrayList<Long> getChildren(long con) throws Exception;

	public boolean hasChildren(long con) throws Exception;

	public ArrayList<Long> getParents(long con) throws Exception;

	/**
	 * Does c1 subsume c2
	 * 
	 * Note: true if c1 == c2
	 * 
	 * @param c1
	 *            an sctid
	 * 
	 * @param c2
	 *            an sctid
	 * 
	 * @return true iff c1 subsumes c2
	 * 
	 * @throws Exception
	 *             exception
	 */
	public boolean isaSubsumesP(long c1, long c2) throws Exception;

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of descendant sctids (does not include con)
	 * 
	 * @throws Exception
	 *             exception
	 */
	public ArrayList<Long> getDescendants(long con) throws Exception;

	/**
	 * @param con
	 *            an sctid
	 * 
	 * @return list of ancestor sctids
	 * 
	 * @throws Exception
	 *             exception
	 */
	public ArrayList<Long> getAncestors(long con) throws Exception;

}
