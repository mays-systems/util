/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

import com.mays.util.BasicLogger;
import com.mays.util.BasicProgressMonitor;
import com.mays.util.BasicProgressMonitorNoOp;

public abstract class SubsetDbBase {

	public static boolean TRACE_P = true;

	protected Connection conn;

	private Long subsets_synch = System.currentTimeMillis();

	private HashMap<UUID, SubsetBasic> subsets;

	public SubsetDbBase(Connection conn) {
		this.conn = conn;
		this.subsets = new HashMap<UUID, SubsetBasic>();
	}

	public void init() throws Exception {
		this.init(true);
	}

	public void init(boolean create_p) throws Exception {
		try {
			this.initSubsetList();
		} catch (Exception ex) {
			if (create_p) {
				SubsetSchema.LOGGER = BasicLogger.createConsoleLogger();
				SubsetSchema s = new SubsetSchema();
				s.init(conn);
				s.createSchema();
				s.createIndexes();
				this.initSubsetList();
			} else {
				throw ex;
			}
		}
	}

	protected void initSubsetList() throws Exception {
		synchronized (subsets_synch) {
			this.subsets = new HashMap<UUID, SubsetBasic>();
			String query = "";
			query += "SELECT id, name, last_modified, built_p ";
			query += "  FROM subset_def ";
			Statement stmt = conn.createStatement();
			ResultSet res = stmt.executeQuery(query);
			while (res.next()) {
				UUID id = UUID.fromString(res.getString(1));
				String name = res.getString(2);
				long last_modified = res.getLong(3);
				int built_p = res.getInt(4);
				SubsetBasic subset = new SubsetBasic(id, name, last_modified, built_p == 1);
				this.subsets.put(id, subset);
			}
			res.close();
			stmt.close();
		}
	}

	public ArrayList<SubsetBasic> getSubsets() throws Exception {
		ArrayList<SubsetBasic> result = new ArrayList<SubsetBasic>();
		synchronized (subsets_synch) {
			for (SubsetBasic subset : this.subsets.values()) {
				result.add(subset);
			}
		}
		return result;
	}

	public SubsetBasic getSubset(UUID id) {
		synchronized (subsets_synch) {
			return this.subsets.get(id);
		}
	}

	public SubsetBasic getSubsetByName(String name) {
		synchronized (subsets_synch) {
			for (SubsetBasic subset : this.subsets.values()) {
				if (name.equals(subset.getName()))
					return subset;
			}
		}
		return null;
	}

	public ArrayList<SubsetExpression> getSubsetExpression(UUID id) throws Exception {
		ArrayList<SubsetExpression> result = new ArrayList<SubsetExpression>();
		String query = "";
		query = "";
		query += "SELECT operator, modifier, concept_p, concept_id, subset_operand_id ";
		query += "  FROM subset_def_expr ";
		query += " WHERE subset_id = ? ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			String operator = res.getString(1);
			String modifier = res.getString(2);
			int concept_p = res.getInt(3);
			long concept_id = res.getLong(4);
			String subset_operand_id = res.getString(5);
			if ((concept_p == 1)) {
				result.add(new SubsetExpression(Operator.valueOf(operator), Modifier.valueOf(modifier), concept_id));
			} else {
				result.add(new SubsetExpression(Operator.valueOf(operator), UUID.fromString(subset_operand_id)));
			}
		}
		res.close();
		stmt.close();
		return result;
	}

	PreparedStatement stmt_is_subset_member = null;

	public boolean isSubsetMember(UUID subset_id, long con) throws Exception {
		if (this.stmt_is_subset_member == null) {
			String query = "";
			query = "";
			query += "SELECT concept_id ";
			query += "  FROM subset_members ";
			query += " WHERE subset_id = ?";
			query += "   AND concept_id = ?";
			this.stmt_is_subset_member = conn.prepareStatement(query);
		}
		stmt_is_subset_member.setString(1, subset_id.toString());
		stmt_is_subset_member.setLong(2, con);
		ResultSet res = stmt_is_subset_member.executeQuery();
		boolean result = false;
		if (res.next()) {
			result = true;
		}
		res.close();
		return result;
	}

	public Set<Long> getSubsetMembers(UUID id) throws Exception {
		Set<Long> result = new HashSet<Long>();
		String query = "";
		query = "";
		query += "SELECT concept_id ";
		query += "  FROM subset_members ";
		query += " WHERE subset_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id = res.getLong(1);
			result.add(concept_id);
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<Long> getChildren(UUID subset_id, long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id1 ";
		query += "  FROM subset_isa ";
		query += " WHERE concept_id2 = ? ";
		query += "   AND subset_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		stmt.setString(2, subset_id.toString());
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	public ArrayList<Long> getParents(UUID subset_id, long con) throws Exception {
		ArrayList<Long> result = new ArrayList<Long>();
		String query = "";
		query += "SELECT concept_id2 ";
		query += "  FROM subset_isa ";
		query += " WHERE concept_id1 = ? ";
		query += "   AND subset_id = ? ";
		PreparedStatement stmt = this.conn.prepareStatement(query);
		stmt.setLong(1, con);
		stmt.setString(2, subset_id.toString());
		ResultSet res = stmt.executeQuery();
		while (res.next()) {
			long concept_id1 = res.getLong(1);
			result.add(concept_id1);
		}
		res.close();
		stmt.close();
		return result;
	}

	public void renameSubset(UUID id, String name) throws Exception {
		renameSubset(id, name, true);
	}

	public void renameSubset(UUID id, String name, boolean commit) throws Exception {
		String query = "";
		query += "UPDATE subset_def ";
		query += "   SET name = ?";
		query += " WHERE id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, name);
		stmt.setString(2, id.toString());
		stmt.executeUpdate();
		stmt.close();
		if (commit)
			conn.commit();
		this.initSubsetList();
	}

	public void setModified(UUID id) throws Exception {
		setModified(id, new Date().getTime(), false);
	}

	public void setModified(UUID id, long last_modified) throws Exception {
		setModified(id, last_modified, false);
	}

	private void setModified(UUID id, long last_modified, boolean built_p) throws Exception {
		String query = "";
		query += "UPDATE subset_def ";
		query += "   SET last_modified = ?";
		query += " WHERE id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setLong(1, last_modified);
		stmt.setString(2, id.toString());
		stmt.executeUpdate();
		stmt.close();
		setBuilt(id, built_p);
		this.initSubsetList();
	}

	private void setBuilt(UUID id, boolean built_p) throws Exception {
		String query = "";
		query += "UPDATE subset_def ";
		query += "   SET built_p = ?";
		query += " WHERE id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, (built_p ? 1 : 0));
		stmt.setString(2, id.toString());
		stmt.executeUpdate();
		stmt.close();
	}

	public void deleteSubset(UUID id) throws Exception {
		deleteSubset(id, true);
	}

	public void deleteSubset(UUID id, boolean commit) throws Exception {
		String query = "";
		query += "DELETE FROM subset_def ";
		query += " WHERE id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		stmt.executeUpdate();
		stmt.close();
		deleteSubsetMembers(id, false);
		if (commit)
			conn.commit();
		this.initSubsetList();
	}

	public void deleteSubsetMembers(UUID id) throws Exception {
		deleteSubsetMembers(id, true);
	};

	private void deleteSubsetMembers(UUID id, boolean modified_p) throws Exception {
		String query = "";
		query += "DELETE FROM subset_members ";
		query += " WHERE subset_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		stmt.executeUpdate();
		stmt.close();
		if (modified_p) {
			setModified(id);
		}
	}

	public void deleteHierarchy(UUID id) throws Exception {
		String query = "";
		query += "DELETE FROM subset_isa ";
		query += " WHERE subset_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		stmt.executeUpdate();
		stmt.close();
	}

	public UUID createSubset(String name, ArrayList<SubsetExpression> exprs, boolean delete_exisitng_p, boolean build_p)
			throws Exception {
		if (delete_exisitng_p) {
			SubsetBasic s = getSubsetByName(name);
			if (s != null) {
				deleteSubset(s.getId());
			}
		}
		UUID id = UUID.randomUUID();
		createSubset(name, id, new Date().getTime(), true);
		saveSubsetExpression(id, exprs);
		if (build_p)
			buildSubset(id);
		return id;
	}

	public UUID createSubset(String name) throws Exception {
		UUID id = UUID.randomUUID();
		createSubset(name, id, new Date().getTime(), true);
		return id;
	}

	public void createSubset(String name, UUID id) throws Exception {
		createSubset(name, id, new Date().getTime(), true);
	}

	public void createSubset(String name, UUID id, long last_modified, boolean commit) throws Exception {
		deleteSubset(id, commit);
		String query = "";
		query += "INSERT INTO subset_def ";
		query += " (id, name, last_modified, built_p)";
		query += " VALUES (?, ?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		stmt.setString(2, name);
		stmt.setLong(3, last_modified);
		stmt.setInt(4, 0);
		stmt.executeUpdate();
		stmt.close();
		if (commit)
			conn.commit();
		this.initSubsetList();
	}

	public void saveSubsetExpression(UUID id, ArrayList<SubsetExpression> exprs) throws Exception {
		String query = "";
		query += "DELETE FROM subset_def_expr ";
		query += " WHERE subset_id = ? ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		stmt.executeUpdate();
		stmt.close();
		query = "";
		query += "INSERT INTO subset_def_expr ";
		query += " (subset_id, operator, modifier, concept_p, concept_id, subset_operand_id )";
		query += " VALUES (?, ?, ?, ?, ?, ?)";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		for (SubsetExpression expr : exprs) {
			stmt.setString(2, expr.getOperator().toString());
			stmt.setString(3, expr.getModifier().toString());
			stmt.setInt(4, (expr.isConcept() ? 1 : 0));
			stmt.setLong(5, expr.getConceptId());
			stmt.setString(6, (expr.isConcept() ? "" : expr.getSubsetId().toString()));
			stmt.executeUpdate();
		}
		stmt.close();
		this.setModified(id);
		conn.commit();
	}

	public void createSubset(String name, UUID id, Set<Long> concepts) throws Exception {
		createSubset(name, id, new Date().getTime(), false);
		this.storeSubset(id, concepts);
		conn.commit();
	}

	public void storeSubset(UUID id, Set<Long> concepts) throws Exception {
		String query = "";
		query += "INSERT INTO subset_members ";
		query += " (subset_id, concept_id )";
		query += " VALUES (?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		for (long con : concepts) {
			stmt.setLong(2, con);
			stmt.executeUpdate();
		}
		stmt.close();
	}

	public void storeHierarchy(UUID id, HashMap<Long, ArrayList<Long>> hierarchy) throws Exception {
		String query = "";
		query += "INSERT INTO subset_isa ";
		query += " (subset_id, concept_id1, concept_id2 )";
		query += " VALUES (?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, id.toString());
		for (Entry<Long, ArrayList<Long>> e : hierarchy.entrySet()) {
			stmt.setLong(2, e.getKey());
			if (e.getValue().size() == 0) {
				e.getValue().add(0l);
			}
			for (long con2 : e.getValue()) {
				stmt.setLong(3, con2);
				stmt.executeUpdate();
			}
		}
		stmt.close();
	}

	public void buildSubset(UUID id) throws Exception {
		this.buildSubset(id, new BasicProgressMonitorNoOp());
	}

	public void buildSubset(UUID id, I_Isa isa) throws Exception {
		this.buildSubset(id, isa, new BasicProgressMonitorNoOp());
	}

	public abstract void buildSubset(UUID id, BasicProgressMonitor mon) throws Exception;

	public void buildSubset(UUID id, I_Isa isa, BasicProgressMonitor mon) throws Exception {
		long beg = System.currentTimeMillis();
		if (TRACE_P)
			System.out.println("buildSubset");
		deleteSubsetMembers(id);
		mon.worked(1);
		deleteHierarchy(id);
		mon.worked(1);
		ArrayList<SubsetExpression> exprs = this.getSubsetExpression(id);
		SubsetFactory f = new SubsetFactory(this, isa);
		if (TRACE_P)
			System.out.println("buildSubset: " + ((System.currentTimeMillis() - beg) // /
																						// 1000
			));
		for (SubsetExpression e : exprs) {
			switch (e.getOperator()) {
			case INCLUDE:
				f.processExpression(e);
				break;
			}
		}
		for (SubsetExpression e : exprs) {
			switch (e.getOperator()) {
			case EXCLUDE:
				f.processExpression(e);
				break;
			case INTERSECT:
				f.processExpression(e);
				break;
			}
		}
		mon.worked(1);
		this.storeSubset(id, f.getMembers());
		mon.worked(1);
		// System.out.println("PUT THIS BACK");
		f.computeHierarchy();
		mon.worked(1);
		this.storeHierarchy(id, f.getHierarchy());
		mon.worked(1);
		this.setBuilt(id, true);
		conn.commit();
		if (TRACE_P)
			System.out.println("buildSubset complete: " + ((System.currentTimeMillis() - beg) // /
																								// 1000
			));
		this.initSubsetList();
	}

}
