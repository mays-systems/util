/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.sql.Connection;
import java.util.logging.Logger;

import com.mays.util.BasicLogger;
import com.mays.util.Config;
import com.mays.util.Sql;

public class SubsetSchema {

	public static Logger LOGGER;

	private Config config;

	private Connection conn;

	public SubsetSchema(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public SubsetSchema() throws Exception {
		super();
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		Sql.shutdown(schema);
	}

	public void createSchema() throws Exception {
		String query;
		//
		Sql.dropTable(conn, "subset_def");
		query = "";
		query += "id CHAR(36) PRIMARY KEY, ";
		query += "name VARCHAR(255), ";
		query += "last_modified BIGINT, ";
		query += "built_p INT ";
		Sql.createTable(conn, "subset_def", query);
		//
		Sql.dropTable(conn, "subset_def_expr");
		query = "";
		query += "subset_id CHAR(36), ";
		query += "operator VARCHAR(255), ";
		query += "modifier VARCHAR(255), ";
		query += "concept_p INT, ";
		query += "concept_id BIGINT, ";
		query += "subset_operand_id CHAR(36) ";
		Sql.createTable(conn, "subset_def_expr", query);
		//
		Sql.dropTable(conn, "subset_members");
		query = "";
		query += "subset_id CHAR(36), ";
		query += "concept_id BIGINT ";
		Sql.createTable(conn, "subset_members", query);
		//
		Sql.dropTable(conn, "subset_isa");
		query = "";
		query += "subset_id CHAR(36), ";
		query += "concept_id1 BIGINT , ";
		query += "concept_id2 BIGINT ";
		Sql.createTable(conn, "subset_isa", query);
		conn.commit();
		LOGGER.info("Created schema");
	}

	public void createIndexes() throws Exception {
		Sql.createIndex(conn, "subset_members", "subset_id");
		Sql.createIndex(conn, "subset_members", "concept_id");
		Sql.createIndex(conn, "subset_isa", "subset_id");
		Sql.createIndex(conn, "subset_isa", "subset_id, concept_id1");
		Sql.createIndex(conn, "subset_isa", "subset_id, concept_id2");
		this.conn.commit();
		LOGGER.info("Created indexes");
	}

	public void run() throws Exception {
		this.init();
		this.createSchema();
		this.createIndexes();
		this.shutdown();
	}

	public static void main(String[] args) {
		try {
			SubsetSchema.LOGGER = BasicLogger.init(SubsetSchema.class, true, true);
			Sql.LOGGER = SubsetSchema.LOGGER;
			String config_name = Config.getConfigName(args, LOGGER);
			SubsetSchema m = new SubsetSchema(config_name);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
