/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.subset;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class SubsetFactory {

	public static Logger LOGGER;

	private Set<Long> members;

	private HashMap<Long, ArrayList<Long>> hierarchy;

	private I_Isa isa;

	private SubsetDbBase subset_db;

	public SubsetFactory(SubsetDbBase subset_db, I_Isa isa) {
		super();
		this.subset_db = subset_db;
		this.isa = isa;
		this.members = new HashSet<Long>();
	}

	public Set<Long> getMembers() {
		return members;
	}

	public void setMembers(Set<Long> members) {
		this.members = members;
	}

	public HashMap<Long, ArrayList<Long>> getHierarchy() {
		return hierarchy;
	}

	public Set<Long> getChildren(long con) throws Exception {
		return this.getChildren(con, true);
	}

	public Set<Long> getChildren(long con, boolean inclusive) throws Exception {
		HashSet<Long> res = new HashSet<Long>();
		res.addAll(this.isa.getChildren(con));
		if (inclusive)
			res.add(con);
		return res;
	}

	public Set<Long> getDescendants(long con) throws Exception {
		return this.getDescendants(con, true);
	}

	public Set<Long> getDescendants(long con, boolean inclusive) throws Exception {
		HashSet<Long> res = new HashSet<Long>();
		res.addAll(this.isa.getDescendants(con));
		if (inclusive)
			res.add(con);
		return res;
	}

	public Set<Long> getDescendantsLeaf(long con) throws Exception {
		HashSet<Long> res = new HashSet<Long>();
		for (long desc : getDescendants(con, true)) {
			if (!this.isa.hasChildren(desc))
				res.add(desc);
		}
		return res;
	}

	public Set<Long> intersect(Set<Long> s1, Set<Long> s2) {
		s1.retainAll(s2);
		return s1;
	}

	public Set<Long> union(Set<Long> s1, Set<Long> s2) {
		s1.addAll(s2);
		return s1;
	}

	public Set<Long> minus(Set<Long> s1, Set<Long> s2) {
		s1.removeAll(s2);
		return s1;
	}

	public void intersect(Set<Long> s2) {
		this.members.retainAll(s2);
	}

	public void union(Set<Long> s2) {
		this.members.addAll(s2);
	}

	public void minus(Set<Long> s2) {
		this.members.removeAll(s2);
	}

	public void processExpression(SubsetExpression expr) throws Exception {
		Set<Long> expr_cons = null;
		if (expr.isConcept()) {
			long con = expr.getConceptId();
			switch (expr.getModifier()) {
			case CHILDREN:
				expr_cons = this.getChildren(con);
				break;
			case CHILDREN_ONLY:
				expr_cons = this.getChildren(con, false);
				break;
			case DESCENDANTS:
				expr_cons = this.getDescendants(con);
				break;
			case DESCENDANTS_ONLY:
				expr_cons = this.getDescendants(con, false);
				break;
			case DESCENDANTS_LEAF:
				expr_cons = this.getDescendantsLeaf(con);
				break;
			case ONLY:
				expr_cons = new HashSet<Long>();
				expr_cons.add(con);
				break;
			}
		} else {
			expr_cons = this.subset_db.getSubsetMembers(expr.getSubsetId());
		}
		switch (expr.getOperator()) {
		case INCLUDE:
			this.union(expr_cons);
			break;
		case EXCLUDE:
			this.minus(expr_cons);
			break;
		case INTERSECT:
			this.intersect(expr_cons);
			break;
		}
	}

	private class SubsetConcept {

		long id;

		List<SubsetConcept> parents = new ArrayList<SubsetConcept>();

		List<SubsetConcept> children = new ArrayList<SubsetConcept>();

		int visited_marker = 0;

		int found_marker = 0;

		public SubsetConcept(long id) {
			super();
			this.id = id;
		}

	}

	private boolean findMss(SubsetConcept con, List<SubsetConcept> potential_mss, int visited_marker,
			List<SubsetConcept> mss) throws Exception {
		boolean found_mss_p = false;
		for (SubsetConcept potential_mss_con : potential_mss) {
			if (findMss(con, potential_mss_con, visited_marker, mss))
				found_mss_p = true;
		}
		return found_mss_p;
	}

	private boolean findMss(SubsetConcept con, SubsetConcept potential_mss_con, int visited_marker,
			List<SubsetConcept> mss) throws Exception {
		if (potential_mss_con.visited_marker == visited_marker)
			return (potential_mss_con.found_marker == visited_marker);
		potential_mss_con.visited_marker = visited_marker;
		if (this.isa.isaSubsumesP(potential_mss_con.id, con.id)) {
			if (findMss(con, potential_mss_con.children, visited_marker, mss)) {
				// Added
				potential_mss_con.found_marker = visited_marker;
				//
				return true;
			}
			mss.add(potential_mss_con);
			potential_mss_con.found_marker = visited_marker;
			return true;
		}
		return false;
	}

	private boolean findMgs(SubsetConcept con, List<SubsetConcept> potential_mgs, int visited_marker,
			List<SubsetConcept> mgs) throws Exception {
		boolean found_mgs_p = false;
		for (SubsetConcept potential_mgs_con : potential_mgs) {
			if (findMgs(con, potential_mgs_con, visited_marker, mgs))
				found_mgs_p = true;
		}
		return found_mgs_p;
	}

	private int mgs_i = 0;

	private void mgsPrint(String line) {
		String line_out = "";
		for (int i = 0; i < mgs_i; i++) {
			line_out += ">";
		}
		line_out += line;
		if (LOGGER != null)
			LOGGER.info(line_out);
	}

	private boolean findMgs(SubsetConcept con, SubsetConcept potential_mgs_con, int visited_marker,
			List<SubsetConcept> mgs) throws Exception {
		mgs_i++;
		if (trace_p) {
			mgsPrint("In findMgs");
			printConcepts(Arrays.asList(con));
			mgsPrint("Potential");
			printConcepts(Arrays.asList(potential_mgs_con));
		}
		if (potential_mgs_con.visited_marker == visited_marker) {
			if (trace_p)
				mgsPrint("In findMgs, visited, return " + (potential_mgs_con.found_marker == visited_marker));
			mgs_i--;
			return (potential_mgs_con.found_marker == visited_marker);
		}
		if (trace_p)
			mgsPrint("In findMgs, not visited");
		potential_mgs_con.visited_marker = visited_marker;
		if (this.isa.isaSubsumesP(con.id, potential_mgs_con.id)) {
			// if (trace_p)
			// mgsPrint("In findMgs, subsumes potential");
			// if (findMgsUp(con, potential_mgs_con.parents, visited_marker,
			// mgs)) {
			// // Added 11:55 AM
			// potential_mgs_con.found_marker = visited_marker;
			// //
			// mgs_i--;
			// return true;
			// }
			if (trace_p) {
				mgsPrint("In findMgs, add mgs:");
				printConcepts(Arrays.asList(potential_mgs_con));
			}
			boolean subsumed_p = false;
			for (SubsetConcept mgs_con : mgs) {
				if (this.isa.isaSubsumesP(mgs_con.id, potential_mgs_con.id)) {
					subsumed_p = true;
					break;
				}
			}
			if (!subsumed_p) {
				ArrayList<SubsetConcept> to_remove = new ArrayList<SubsetConcept>();
				for (SubsetConcept mgs_con : mgs) {
					if (this.isa.isaSubsumesP(potential_mgs_con.id, mgs_con.id))
						to_remove.add(mgs_con);
				}
				mgs.removeAll(to_remove);
				mgs.add(potential_mgs_con);
			}
			potential_mgs_con.found_marker = visited_marker;
			mgs_i--;
			return true;
		}
		if (trace_p)
			mgsPrint("In findMgs, searching children");
		{
			boolean ret = findMgs(con, potential_mgs_con.children, visited_marker, mgs);
			// Added 11:58 AM
			if (ret)
				potential_mgs_con.found_marker = visited_marker;
			//
			mgs_i--;
			return ret;
		}
	}

	private boolean findMgsUp(SubsetConcept con, List<SubsetConcept> potential_mgs, int visited_marker,
			List<SubsetConcept> mgs) throws Exception {
		boolean found_mgs_p = false;
		for (SubsetConcept potential_mgs_con : potential_mgs) {
			if (findMgsUp(con, potential_mgs_con, visited_marker, mgs))
				found_mgs_p = true;
		}
		return found_mgs_p;
	}

	private int mgs_up_i = 0;

	private void mgsUpPrint(String line) {
		String line_out = "";
		for (int i = 0; i < mgs_up_i; i++) {
			line_out += "-";
		}
		line_out += line;
		if (LOGGER != null)
			LOGGER.info(line_out);
	}

	private boolean findMgsUp(SubsetConcept con, SubsetConcept potential_mgs_con, int visited_marker,
			List<SubsetConcept> mgs) throws Exception {
		mgs_up_i++;
		if (trace_p) {
			mgsUpPrint("In findMgsUp");
			printConcepts(Arrays.asList(con));
			mgsUpPrint("Potential");
			printConcepts(Arrays.asList(potential_mgs_con));
		}
		if (potential_mgs_con.visited_marker == visited_marker) {
			if (trace_p)
				mgsUpPrint("In findMgsUp, visited, return " + (potential_mgs_con.found_marker == visited_marker));
			mgs_up_i--;
			return (potential_mgs_con.found_marker == visited_marker);
		}
		if (trace_p)
			mgsUpPrint("In findMgsUp, not visited");
		potential_mgs_con.visited_marker = visited_marker;
		if (this.isa.isaSubsumesP(con.id, potential_mgs_con.id)) {
			if (trace_p)
				mgsUpPrint("In findMgsUp, subsumes potential");
			if (findMgsUp(con, potential_mgs_con.parents, visited_marker, mgs)) {
				// Added 12/11/10
				potential_mgs_con.found_marker = visited_marker;
				// Added end
				mgs_up_i--;
				return true;
			}
			if (trace_p) {
				mgsUpPrint("In findMgsUp, add mgs:");
				printConcepts(Arrays.asList(potential_mgs_con));
			}
			mgs.add(potential_mgs_con);
			potential_mgs_con.found_marker = visited_marker;
			mgs_up_i--;
			return true;
		}
		if (trace_p)
			mgsUpPrint("In findMgsUp 4");
		mgs_up_i--;
		return false;
	}

	boolean trace_p = false;

	void printConcepts(List<SubsetConcept> cons) throws Exception {
		for (SubsetConcept con : cons) {
			if (LOGGER != null)
				LOGGER.info("\t" + isa.getLogString(con.id));
		}
	}

	// For Entire tooth
	// List<Long> traced_id = Arrays.asList(422032003l);

	// List<Long> traced_id = Arrays.asList(94209000l);

	List<Long> traced_id = Collections.emptyList();

	public void computeHierarchy() throws Exception {
		List<SubsetConcept> roots = new ArrayList<SubsetConcept>();
		List<SubsetConcept> concepts = new ArrayList<SubsetConcept>();
		int marker = 0;
		int cnt = 0;
		ArrayList<Long> sorted_members = new ArrayList<Long>(this.members);
		Collections.sort(sorted_members);
		for (long id : sorted_members) {
			cnt++;
			if ((cnt % 1000) == 0)
				System.out.println("Processed " + cnt + " of " + this.members.size());
			trace_p = traced_id.contains(id);
			SubsetConcept con = new SubsetConcept(id);
			if (trace_p) {
				if (LOGGER != null)
					LOGGER.info("Tracing");
				printConcepts(Arrays.asList(con));
				for (SubsetConcept sc : concepts) {
					LOGGER.info("Concept:");
					printConcepts(Arrays.asList(sc));
					LOGGER.info("Parents:");
					printConcepts(sc.parents);
				}
			}
			concepts.add(con);
			if (roots.size() == 0) {
				roots.add(con);
				continue;
			}
			List<SubsetConcept> mss = new ArrayList<SubsetConcept>();
			boolean found_mss = findMss(con, roots, ++marker, mss);
			if (trace_p) {
				if (LOGGER != null)
					LOGGER.info("MSS " + found_mss + " " + mss.size());
				printConcepts(mss);
			}
			List<SubsetConcept> mgs = new ArrayList<SubsetConcept>();
			findMgs(con, (mss.size() == 0 ? roots : mss), ++marker, mgs);
			if (trace_p) {
				if (LOGGER != null)
					LOGGER.info("MGS " + mgs.size());
				printConcepts(mgs);
			}
			if (!found_mss) {
				roots.add(con);
				if (mss.size() != 0)
					throw new Exception("mss bogosity");
			} else {
				if (mss.size() == 0)
					throw new Exception("mss bogosity");
				for (SubsetConcept mss_con : mss) {
					if (traced_id.contains(mss_con.id)) {
						if (LOGGER != null)
							LOGGER.info("Concept");
						printConcepts(Arrays.asList(con));
						if (LOGGER != null)
							LOGGER.info("has MSS");
						printConcepts(mss);
					}
					con.parents.add(mss_con);
					mss_con.children.add(con);
				}
			}
			for (SubsetConcept mgs_con : mgs) {
				if (traced_id.contains(mgs_con.id)) {
					if (LOGGER != null)
						LOGGER.info("Concept");
					printConcepts(Arrays.asList(con));
					if (LOGGER != null)
						LOGGER.info("has MGS");
					printConcepts(mgs);
				}
				if (roots.contains(mgs_con))
					roots.remove(mgs_con);
				ArrayList<SubsetConcept> to_remove = new ArrayList<SubsetConcept>();
				for (SubsetConcept p : mgs_con.parents) {
					if (con.parents.contains(p)) {
						to_remove.add(p);
					}
				}
				for (SubsetConcept p : to_remove) {
					mgs_con.parents.remove(p);
					p.children.remove(mgs_con);
				}
				con.children.add(mgs_con);
				mgs_con.parents.add(con);
			}
			if (trace_p) {
				if (LOGGER != null)
					LOGGER.info("End trace");
				printConcepts(Arrays.asList(con));
				if (LOGGER != null)
					LOGGER.info("----------");
			}
		}
		this.hierarchy = new HashMap<Long, ArrayList<Long>>();
		for (SubsetConcept con : concepts) {
			ArrayList<Long> parents = new ArrayList<Long>();
			// System.out.println(this.snomed_isa.getConcept(con.id)
			// .getFullySpecifiedName());
			for (SubsetConcept p : con.parents) {
				parents.add(p.id);
				// System.out.println("\t"
				// + this.snomed_isa.getConcept(p.id)
				// .getFullySpecifiedName());
			}
			this.hierarchy.put(con.id, parents);
		}
	}

}
