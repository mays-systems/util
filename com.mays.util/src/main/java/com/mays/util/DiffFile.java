/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.BufferedReader;
import java.util.HashSet;
import java.util.logging.Logger;

public class DiffFile {

	public static Logger LOGGER;

	private String file1;

	private String file2;

	public DiffFile(String file1, String file2) throws Exception {
		super();
		this.file1 = file1;
		this.file2 = file2;
	}

	public HashSet<String> readFile(String file_name) throws Exception {
		HashSet<String> res = new HashSet<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file_name);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			res.add(line);
		}
		return res;
	}

	public void diff(HashSet<String> f1, HashSet<String> f2, String tag) {
		for (String s1 : f1) {
			if (!(f2.contains(s1)))
				LOGGER.warning(s1 + "\t" + tag);
		}
	}

	public void run() throws Exception {
		HashSet<String> f1 = readFile(file1);
		HashSet<String> f2 = readFile(file2);
		LOGGER.info("1-2");
		diff(f1, f2, "1-2");
		LOGGER.info("2-1");
		diff(f2, f1, "2-1");
	}

}
