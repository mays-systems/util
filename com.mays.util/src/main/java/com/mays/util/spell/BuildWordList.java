/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.spell;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import com.mays.util.BasicLogger;
import com.mays.util.IO;
import com.mays.util.Sql;

public class BuildWordList {

	public static Logger LOGGER;

	public static String MOBY_DIR = "/data/Moby/mwords/";

	String moby_common = "74550common.txt";

	String moby_single = "354984singl.txt";

	String moby_xwords = "113809of.fic";

	public static String DICTS12_DIR = "/data/12dicts-5.0/";

	String dict12_2of12inf = "2of12inf.txt";

	String dict12_2of12 = "2of12.txt";

	public static String SNOMED_DIR = "/data/snomed/SnomedCT_USEditionRF2_Production_20170301T120000/Snapshot/Terminology/";

	String snomed_descriptions = "sct2_Description_Snapshot-en_US1000124_20170301.txt";

	public static String CPT_DIR = "/Users/emays/projects/cpt/work/cptxml2018/cptdtkexport/";

	String cpt_property = "Property.txt";

	boolean log_p = false;

	public SortedSet<String> readWords(String file) throws Exception {
		SortedSet<String> words = new TreeSet<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			line = line.trim();
			words.add(line);
		}
		LOGGER.info("Words: " + file + " " + words.size());
		return words;
	}

	public SortedSet<String> readWordsDict12(String file) throws Exception {
		SortedSet<String> words = new TreeSet<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			line = line.trim();
			if (line.matches("[A-Z]+"))
				continue;
			if (line.contains(" "))
				continue;
			if (line.contains("-"))
				continue;
			if (line.endsWith(":") || line.endsWith("&") || line.endsWith("+"))
				continue;
			line = line.replaceAll("[=#^%]$", "");
			if (!line.matches("[a-z']+")) {
				if (log_p)
					LOGGER.info(line);
			} else {
				words.add(line);
			}
		}
		LOGGER.info("Lines: " + file + " " + line_i);
		LOGGER.info("Words: " + file + " " + words.size());
		return words;
	}

	// Copied from com.mays.snomed.Snomed

	private enum CaseSensitive {
		CASE_SENSITIVE(900000000000017005l, "Case sensitive"),
		//
		CASE_INSENSITIVE_INITIAL(900000000000020002l, "Only initial character case insensitive"),
		//
		CASE_INSENSITIVE(900000000000448009l, "Case insensitive");

		public long id;

		public String name;

		private CaseSensitive(long id, String name) {
			this.id = id;
			this.name = name;
		}

		public static CaseSensitive getCaseSensitive(long type) {
			for (CaseSensitive t : values()) {
				if (t.id == type)
					return t;
			}
			return null;
		}

		public static CaseSensitive getCaseSensitive(String type) {
			for (CaseSensitive t : values()) {
				if (t.name.equals(type))
					return t;
			}
			return null;
		}
	}

	// id effectiveTime active moduleId conceptId languageCode typeId term
	// caseSignificanceId

	public SortedSet<String> readSnomed(String file) throws Exception {
		SortedSet<String> words = new TreeSet<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\t");
			// active
			if (!field[2].equals("1"))
				continue;
			// term
			String term = field[7];
			long case_sig = Long.parseLong(field[8]);
			CaseSensitive cs = CaseSensitive.getCaseSensitive(case_sig);
			if (cs == null)
				throw new Exception("" + case_sig);
			switch (cs) {
			case CASE_INSENSITIVE:
				term = term.toLowerCase();
				break;
			case CASE_INSENSITIVE_INITIAL:
				term = Spell.toInitialLowerCase(term);
				break;
			case CASE_SENSITIVE:
				break;
			default:
				break;
			}
			// language_code
			// TODO
			// if (!Arrays.asList("en", "en-US").contains(field[6]))
			// continue;
			for (String word : Spell.getWords(term)) {
				// if (word.matches("[A-Za-z\\-]+"))
				words.add(word);
			}
		}
		LOGGER.info("Lines: " + file + " " + line_i);
		LOGGER.info("Words: " + file + " " + words.size());
		return words;
	}

	public SortedSet<String> readCpt(String file) throws Exception {
		SortedSet<String> words = new TreeSet<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (line_i == 1)
				continue;
			String[] field = line.split("\\|");
			// Concept ID|Property Type ID|Property Value
			if (!field[1].equals("106"))
				continue;
			String descr = field[2];
			for (String word : Spell.getWords(descr)) {
				words.add(word);
				words.add(word.toLowerCase());
			}
		}
		LOGGER.info("Lines: " + file + " " + line_i);
		LOGGER.info("Words: " + file + " " + words.size());
		return words;
	}

	public void writeWords(Collection<String> words, String file) throws Exception {
		PrintWriter out = IO.createPrintUtf8FileWriter(file);
		for (String word : words) {
			out.println(word);
		}
		out.close();
		LOGGER.info("Words: " + file + " " + words.size());
	}

	public void run() throws Exception {
		String words_file = "/data/spell/words.txt";
		SortedSet<String> moby_words = readWords(MOBY_DIR + moby_xwords);
		SortedSet<String> words = readWordsDict12(DICTS12_DIR + dict12_2of12inf);
		int cnt = 0;
		for (String word : words) {
			if (!moby_words.contains(word)) {
				if (log_p)
					LOGGER.warning(word);
				cnt++;
			}
		}
		LOGGER.info("Diff: " + cnt);
		words.retainAll(moby_words);
		writeWords(words, words_file);
		SortedSet<String> snomed_words = readSnomed(SNOMED_DIR + snomed_descriptions);
		snomed_words.removeAll(words);
		writeWords(snomed_words, "/data/spell/snomed_words.txt");
		SortedSet<String> cpt_words = readCpt(CPT_DIR + cpt_property);
		cpt_words.removeAll(words);
		writeWords(cpt_words, "/data/spell/cpt_words.txt");
	}

	public static void main(String[] args) {
		try {
			BuildWordList.LOGGER = BasicLogger.init(BuildWordList.class, false, false);
			Sql.LOGGER = BuildWordList.LOGGER;
			LOGGER.info("START");
			BuildWordList m = new BuildWordList();
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
