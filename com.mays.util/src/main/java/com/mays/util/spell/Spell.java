/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.spell;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import com.mays.util.IO;

public class Spell {

	public static final String NON_WORD_CHAR_CLASS = "[\\P{L}&&[^\\-\\']]";

	public static Logger LOGGER;

	public static boolean TRACE_P = false;

	HashSet<String> words;

	private static void log(String str) {
		if (LOGGER != null) {
			LOGGER.info(str);
		} else {
			System.out.println(str);
		}
	}

	public Spell() {
		super();
		this.words = new HashSet<String>();
	}

	public Spell(Collection<String> words) {
		super();
		this.words = new HashSet<String>(words);
	}

	public void addWords(Collection<String> word_list) {
		words.addAll(word_list);
	}

	public boolean isWord(String word) {
		word = trimWord(word);
		if (word.equals(""))
			return true;
		if (words.contains(word))
			return true;
		if (words.contains(toInitialLowerCase(word)))
			return true;
		if (word.matches("\\$?\\#?\\d+(\\,\\d+)*\\.?\\d*%?"))
			return true;
		if (word.matches(".*[\\-/].*")) {
			boolean found_all = true;
			for (String w : word.split("[\\-/]")) {
				if (!isWord(w)) {
					found_all = false;
					break;
				}
			}
			if (found_all)
				return true;
		}
		return false;
	}

	public static String trimWord(String word) {
		String orig = word;
		word = word.trim();
		word = trimPlu(word);
		final String chars = "[\\(\\)\\[\\]\\{\\}\\'\"]";
		word = word.replaceAll("^" + chars, "");
		word = word.replaceAll(chars + "$", "");
		word = trimPoss(word);
		word = word.replaceAll("[\\,\\.\\!\\?\\;\\:\\-]$", "");
		if (orig.equals(word))
			return word;
		return trimWord(word);
	}

	public static String trimPoss(String word) {
		word = word.replaceAll("'$", "");
		word = word.replaceAll("'s$", "");
		return word;
	}

	public static String trimPlu(String word) {
		word = word.replaceAll("\\(s\\)$", "");
		return word;
	}

	public static String toInitialLowerCase(String word) {
		if (word.length() <= 1)
			return word.toLowerCase();
		return word.substring(0, 1).toLowerCase() + word.substring(1);
	}

	public static boolean isWordPattern(String word) {
		if (word.equals(""))
			return false;
		if (word.matches("\\p{Punct}+"))
			return false;
		if (word.matches("\\d+"))
			return false;
		if (word.matches("\\d\\.\\d+"))
			return false;
		return true;
	}

	public static List<String> getWords(String text) {
		ArrayList<String> words = new ArrayList<String>();
		BreakIterator boundary = BreakIterator.getWordInstance();
		boundary.setText(text);
		int start = boundary.first();
		for (int end = boundary.next(); end != BreakIterator.DONE; start = end, end = boundary.next()) {
			String word = text.substring(start, end).trim();
			if (isWordPattern(word))
				words.add(word);
		}
		return words;
	}

	public static ArrayList<String> readWords(String file_name) throws IOException {
		ArrayList<String> words = new ArrayList<String>();
		BufferedReader in = IO.createBufferedUtf8FileReader(file_name);
		String line;
		while ((line = in.readLine()) != null) {
			words.add(line.trim());
		}
		return words;
	}

	public static int editDistance(String s1, String s2) {

		int[][] d = new int[s1.length() + 1][s2.length() + 1];

		for (int i = 0; i < s1.length() + 1; i++)
			d[i][0] = i;

		for (int j = 0; j < s2.length() + 1; j++)
			d[0][j] = j;

		for (int j = 1; j < s2.length() + 1; j++) {
			for (int i = 1; i < s1.length() + 1; i++) {
				int sub = 0;
				if (s1.charAt(i - 1) != s2.charAt(j - 1))
					sub++;
				d[i][j] = Math.min(d[i - 1][j] + 1, Math.min(d[i][j - 1] + 1, d[i - 1][j - 1] + sub));

			}
		}

		return d[s1.length()][s2.length()];
	}

	public enum Ops {

		Add(), Del(), Sub(), Eq()
	}

	public static class Alignment {

		public int distance;

		public String a1, a2;

		public Alignment(int distance, String a1, String a2) {
			super();
			this.distance = distance;
			this.a1 = a1;
			this.a2 = a2;
		}

	}

	public static class AlignmentList {

		public int distance;

		public ArrayList<String> a1, a2;

		public AlignmentList(int distance, ArrayList<String> a1, ArrayList<String> a2) {
			super();
			this.distance = distance;
			this.a1 = a1;
			this.a2 = a2;
		}

	}

	public static class AlignmentWords {

		public double distance;

		public ArrayList<String> a1, a2;

		public AlignmentWords(double d, ArrayList<String> a1, ArrayList<String> a2) {
			super();
			this.distance = d;
			this.a1 = a1;
			this.a2 = a2;
		}

	}

	public static Alignment editDistanceAlign(String s1, String s2) {

		Ops[][] a = new Ops[s1.length() + 1][s2.length() + 1];

		int[][] d = new int[s1.length() + 1][s2.length() + 1];

		for (int i = 0; i < s1.length() + 1; i++) {
			d[i][0] = i;
			a[i][0] = Ops.Del;
		}

		for (int j = 0; j < s2.length() + 1; j++) {
			d[0][j] = j;
			a[0][j] = Ops.Add;
		}

		for (int i = 1; i < s1.length() + 1; i++) {
			for (int j = 1; j < s2.length() + 1; j++) {
				int sub = 0;
				if (s1.charAt(i - 1) != s2.charAt(j - 1))
					sub++;
				d[i][j] = Math.min(d[i - 1][j] + 1, Math.min(d[i][j - 1] + 1, d[i - 1][j - 1] + sub));
				if (d[i][j] == d[i - 1][j - 1] + sub) {
					a[i][j] = Ops.Sub;
					continue;
				}
				if (d[i][j] == d[i - 1][j] + 1) {
					a[i][j] = Ops.Del;
					continue;
				}
				if (d[i][j] == d[i][j - 1] + 1) {
					a[i][j] = Ops.Add;
					continue;
				}
				throw new RuntimeException("Error " + i + " " + j);
			}
		}

		if (TRACE_P) {
			for (int i = 0; i < s1.length() + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length() + 1; j++) {
					line += d[i][j] + " ";
				}
				log(line);
			}

			for (int i = 0; i < s1.length() + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length() + 1; j++) {
					line += a[i][j] + " ";
				}
				log(line);
			}
		}

		int i = s1.length();
		int j = s2.length();

		String a1r = "";
		String a2r = "";
		int cnt = 0;
		while (i >= 0 && j >= 0) {
			cnt++;
			if (cnt > s1.length() + s2.length())
				throw new RuntimeException("Loop");
			switch (a[i][j]) {
			case Sub:
				a1r += s1.charAt(i - 1);
				a2r += s2.charAt(j - 1);
				i--;
				j--;
				break;
			case Add:
				if (j > 0) {
					a1r += " ";
					a2r += s2.charAt(j - 1);
				}
				j--;
				break;
			case Del:
				if (i > 0) {
					a1r += s1.charAt(i - 1);
					a2r += " ";
				}
				i--;
				break;
			default:
				throw new RuntimeException(a[i][j].toString());
			}
		}
		String a1 = "";
		for (int ci = a1r.length(); ci > 0; ci--) {
			a1 += a1r.charAt(ci - 1);
		}
		String a2 = "";
		for (int ci = a2r.length(); ci > 0; ci--) {
			a2 += a2r.charAt(ci - 1);
		}
		if (TRACE_P) {
			log(s1);
			log(s2);
			log(a1);
			log(a2);
			log("");
		}

		return new Alignment(d[s1.length()][s2.length()], a1, a2);
	}

	/**
	 * @param s1
	 * @param s2
	 * @param no_substitution_p
	 *            do not allow substitution
	 * @return
	 * @throws Exception
	 */
	public static AlignmentList editDistanceAlign(String[] s1, String[] s2, boolean no_substitution_p) {

		Ops[][] a = new Ops[s1.length + 1][s2.length + 1];

		int[][] d = new int[s1.length + 1][s2.length + 1];

		for (int i = 0; i < s1.length + 1; i++) {
			d[i][0] = i;
			a[i][0] = Ops.Del;
		}

		for (int j = 0; j < s2.length + 1; j++) {
			d[0][j] = j;
			a[0][j] = Ops.Add;
		}

		for (int i = 1; i < s1.length + 1; i++) {
			for (int j = 1; j < s2.length + 1; j++) {
				int sub_cost = 0;
				int add_cost = 1;
				int del_cost = 1;
				if (!s1[i - 1].equals(s2[j - 1]))
					sub_cost = (no_substitution_p ? Math.max(s1.length, s2.length) : 1);
				d[i][j] = Math.min(d[i - 1][j] + del_cost,
						Math.min(d[i][j - 1] + add_cost, d[i - 1][j - 1] + sub_cost));
				if (d[i][j] == d[i - 1][j - 1] + sub_cost) {
					a[i][j] = Ops.Sub;
					continue;
				}
				if (d[i][j] == d[i - 1][j] + del_cost) {
					a[i][j] = Ops.Del;
					continue;
				}
				if (d[i][j] == d[i][j - 1] + add_cost) {
					a[i][j] = Ops.Add;
					continue;
				}
				throw new RuntimeException("Error " + i + " " + j);
			}
		}

		if (TRACE_P) {
			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += d[i][j] + "\t";
				}
				log(line);
			}

			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += a[i][j] + " ";
				}
				log(line);
			}
		}

		int i = s1.length;
		int j = s2.length;

		ArrayList<String> a1r = new ArrayList<String>();
		ArrayList<String> a2r = new ArrayList<String>();
		int cnt = 0;
		while (i >= 0 && j >= 0) {
			cnt++;
			if (cnt > s1.length + s2.length)
				throw new RuntimeException("Loop");
			switch (a[i][j]) {
			case Sub:
				a1r.add(s1[i - 1]);
				a2r.add(s2[j - 1]);
				i--;
				j--;
				break;
			case Add:
				if (j > 0) {
					a1r.add("*");
					a2r.add(s2[j - 1]);
				}
				j--;
				break;
			case Del:
				if (i > 0) {
					a1r.add(s1[i - 1]);
					a2r.add("*");
				}
				i--;
				break;
			default:
				throw new RuntimeException(a[i][j].toString());
			}
		}
		ArrayList<String> a1 = new ArrayList<String>();
		for (int ci = a1r.size(); ci > 0; ci--) {
			a1.add(a1r.get(ci - 1));
		}
		ArrayList<String> a2 = new ArrayList<String>();
		for (int ci = a2r.size(); ci > 0; ci--) {
			a2.add(a2r.get(ci - 1));
		}
		if (TRACE_P) {
			log(Arrays.asList(s1).toString());
			log(Arrays.asList(s2).toString());
			log("Distance: " + d[s1.length][s2.length]);
			for (int ci = 0; ci < a1.size(); ci++) {
				log(a1.get(ci) + "\t" + a2.get(ci));
			}
			log(a1.toString());
			log(a2.toString());
			log("");
		}

		return new AlignmentList(d[s1.length][s2.length], a1, a2);
	}

	public static AlignmentList longestCommonSubSequenceAlign(String[] s1, String[] s2) {
		if (TRACE_P)
			log("longestCommonSubSequenceAlign");

		Ops[][] a = new Ops[s1.length + 1][s2.length + 1];

		int[][] d = new int[s1.length + 1][s2.length + 1];

		for (int i = 0; i < s1.length + 1; i++) {
			d[i][0] = 0;
			a[i][0] = Ops.Del;
		}

		for (int j = 0; j < s2.length + 1; j++) {
			d[0][j] = 0;
			a[0][j] = Ops.Add;
		}

		for (int i = 1; i < s1.length + 1; i++) {
			for (int j = 1; j < s2.length + 1; j++) {
				if (s1[i - 1].equals(s2[j - 1])) {
					d[i][j] = d[i - 1][j - 1] + 1;
					a[i][j] = Ops.Sub;
				} else {
					d[i][j] = Math.max(d[i - 1][j], d[i][j - 1]);
					// Switched
					if (d[i][j] == d[i][j - 1]) {
						a[i][j] = Ops.Add;
						continue;
					}
					if (d[i][j] == d[i - 1][j]) {
						a[i][j] = Ops.Del;
						continue;
					}
					throw new RuntimeException("Error " + i + " " + j);
				}
			}
		}

		if (TRACE_P) {
			log(Arrays.asList(s1).toString());
			log(Arrays.asList(s2).toString());
			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += d[i][j] + "\t";
				}
				log(line);
			}

			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += a[i][j] + " ";
				}
				log(line);
			}
		}

		int i = s1.length;
		int j = s2.length;

		ArrayList<String> a1r = new ArrayList<String>();
		ArrayList<String> a2r = new ArrayList<String>();
		int cnt = 0;
		while (i >= 0 && j >= 0) {
			if (cnt > s1.length + s2.length)
				throw new RuntimeException("Loop");
			cnt++;
			switch (a[i][j]) {
			case Sub:
				a1r.add(s1[i - 1]);
				a2r.add(s2[j - 1]);
				i--;
				j--;
				break;
			case Add:
				if (j > 0) {
					a1r.add("*");
					a2r.add(s2[j - 1]);
				}
				j--;
				break;
			case Del:
				if (i > 0) {
					a1r.add(s1[i - 1]);
					a2r.add("*");
				}
				i--;
				break;
			default:
				throw new RuntimeException(a[i][j].toString());
			}
		}
		ArrayList<String> a1 = new ArrayList<String>();
		for (int ci = a1r.size(); ci > 0; ci--) {
			a1.add(a1r.get(ci - 1));
		}
		ArrayList<String> a2 = new ArrayList<String>();
		for (int ci = a2r.size(); ci > 0; ci--) {
			a2.add(a2r.get(ci - 1));
		}
		if (TRACE_P) {
			log(Arrays.asList(s1).toString());
			log(Arrays.asList(s2).toString());
			log("Distance: " + d[s1.length][s2.length]);
			for (int ci = 0; ci < a1.size(); ci++) {
				log(a1.get(ci) + "\t" + a2.get(ci));
			}
			log(a1.toString());
			log(a2.toString());
			log("");
		}

		return new AlignmentList(d[s1.length][s2.length], a1, a2);
	}

	public static AlignmentWords editDistanceWordsAlign(String[] s1, String[] s2) {

		Ops[][] a = new Ops[s1.length + 1][s2.length + 1];

		int[][] d = new int[s1.length + 1][s2.length + 1];

		for (int i = 0; i < s1.length + 1; i++) {
			d[i][0] = i;
			a[i][0] = Ops.Del;
		}

		for (int j = 0; j < s2.length + 1; j++) {
			d[0][j] = j;
			a[0][j] = Ops.Add;
		}

		for (int i = 1; i < s1.length + 1; i++) {
			for (int j = 1; j < s2.length + 1; j++) {
				int sub = 0;
				int add_cost = 1; // 100;// s2[j - 1].length();
				int del_cost = 1; // s1[i - 1].length();
				if (!s1[i - 1].equals(s2[j - 1]))
					// sub = (double) editDistance(s1[i - 1], s2[j - 1]);
					sub = 2;
				// / (double) Math.max(s1[i - 1].length(),
				// s2[j - 1].length());
				d[i][j] = Math.min(d[i - 1][j] + del_cost, Math.min(d[i][j - 1] + add_cost, d[i - 1][j - 1] + sub));
				if (d[i][j] == d[i - 1][j - 1] + sub) {
					a[i][j] = Ops.Sub;
					continue;
				}
				if (d[i][j] == d[i - 1][j] + del_cost) {
					a[i][j] = Ops.Del;
					continue;
				}
				if (d[i][j] == d[i][j - 1] + add_cost) {
					a[i][j] = Ops.Add;
					continue;
				}
				throw new RuntimeException("Error " + i + " " + j);
			}
		}

		if (TRACE_P) {
			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += (int) (d[i][j] * 10) + "\t";
				}
				log(line);
			}

			for (int i = 0; i < s1.length + 1; i++) {
				String line = "";
				for (int j = 0; j < s2.length + 1; j++) {
					line += a[i][j] + " ";
				}
				log(line);
			}
		}

		int i = s1.length;
		int j = s2.length;

		ArrayList<String> a1r = new ArrayList<String>();
		ArrayList<String> a2r = new ArrayList<String>();
		int cnt = 0;
		while (i >= 0 && j >= 0) {
			cnt++;
			if (cnt > s1.length + s2.length)
				throw new RuntimeException("Loop");
			switch (a[i][j]) {
			case Sub:
				a1r.add(s1[i - 1]);
				a2r.add(s2[j - 1]);
				i--;
				j--;
				break;
			case Add:
				if (j > 0) {
					a1r.add("*");
					a2r.add(s2[j - 1]);
				}
				j--;
				break;
			case Del:
				if (i > 0) {
					a1r.add(s1[i - 1]);
					a2r.add("*");
				}
				i--;
				break;
			default:
				throw new RuntimeException(a[i][j].toString());
			}
		}
		ArrayList<String> a1 = new ArrayList<String>();
		for (int ci = a1r.size(); ci > 0; ci--) {
			a1.add(a1r.get(ci - 1));
		}
		ArrayList<String> a2 = new ArrayList<String>();
		for (int ci = a2r.size(); ci > 0; ci--) {
			a2.add(a2r.get(ci - 1));
		}
		if (TRACE_P) {
			log(Arrays.asList(s1).toString());
			log(Arrays.asList(s2).toString());
			log("Distance: " + d[s1.length][s2.length]);
			for (int ci = 0; ci < a1.size(); ci++) {
				log(a1.get(ci) + "\t" + a2.get(ci));
			}
			log(a1.toString());
			log(a2.toString());
			log("");
		}

		return new AlignmentWords(d[s1.length][s2.length], a1, a2);
	}

	public static int editDistance(String[] s1, String[] s2) {

		int[][] d = new int[s1.length + 1][s2.length + 1];

		for (int i = 0; i < s1.length + 1; i++)
			d[i][0] = i;

		for (int j = 0; j < s2.length + 1; j++)
			d[0][j] = j;

		for (int j = 1; j < s2.length + 1; j++) {
			for (int i = 1; i < s1.length + 1; i++) {
				int sub = 0;
				if (!s1[i - 1].equals(s2[j - 1]))
					sub++;
				d[i][j] = Math.min(d[i - 1][j] + 1, Math.min(d[i][j - 1] + 1, d[i - 1][j - 1] + sub));
			}
		}

		return d[s1.length][s2.length];
	}

	public static double editDistanceWords(String[] s1, String[] s2) {

		double[][] d = new double[s1.length + 1][s2.length + 1];

		for (int i = 0; i < s1.length + 1; i++)
			d[i][0] = i;

		for (int j = 0; j < s2.length + 1; j++)
			d[0][j] = j;

		for (int j = 1; j < s2.length + 1; j++) {
			for (int i = 1; i < s1.length + 1; i++) {
				double sub = 0;
				if (!s1[i - 1].equals(s2[j - 1]))
					sub = (double) editDistance(s1[i - 1], s2[j - 1])
							/ (double) Math.max(s1[i - 1].length(), s2[j - 1].length());
				d[i][j] = Math.min(d[i - 1][j] + 1, Math.min(d[i][j - 1] + 1, d[i - 1][j - 1] + sub));
			}
		}

		return d[s1.length][s2.length];
	}

	public class Suggestion {
		public String word;
		public int distance;

		public Suggestion(String word, int distance) {
			super();
			this.word = word;
			this.distance = distance;
		}

	}

	public List<Suggestion> suggestWords(String word) {
		return suggestWords(word, 10);
	}

	public List<Suggestion> suggestWords(String word, int limit) {
		ArrayList<Suggestion> d1 = new ArrayList<Suggestion>();
		ArrayList<Suggestion> d2 = new ArrayList<Suggestion>();
		ArrayList<Suggestion> d3 = new ArrayList<Suggestion>();
		for (String word2 : words) {
			int d = editDistance(word, word2);
			switch (d) {
			case 1:
				d1.add(new Suggestion(word2, d));
				break;
			case 2:
				d2.add(new Suggestion(word2, d));
				break;
			case 3:
				d3.add(new Suggestion(word2, d));
				break;
			}
			if (d1.size() == limit)
				return d1;
		}
		d1.addAll(d2);
		if (d1.size() < limit)
			d1.addAll(d3);
		return d1.subList(0, Math.min(d1.size(), limit));
	}

}
