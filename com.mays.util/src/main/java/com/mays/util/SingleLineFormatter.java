/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 * Log on a single line with less verbosity.
 * 
 * @author Eric Mays
 * 
 */
public class SingleLineFormatter extends SimpleFormatter {

	private static final DateFormat date_format = new SimpleDateFormat(
			"HH:mm:ss");

	private boolean show_method_p = true;

	public void setShowMethod(boolean show_method_p) {
		this.show_method_p = show_method_p;
	}

	public SingleLineFormatter() {
	}

	public SingleLineFormatter(boolean show_method_p) {
		super();
		this.show_method_p = show_method_p;
	}

	/**
	 * Show 24hr time, level, message, and class.method.
	 * 
	 * Tabs appear around the message for easier post processing and console
	 * reading.
	 * 
	 * @see java.util.logging.SimpleFormatter#format(java.util.logging.LogRecord)
	 */
	@Override
	public synchronized String format(LogRecord rec) {
		return date_format.format(rec.getMillis())
				+ " "
				+ rec.getLevel()
				+ "\t"
				+ super.formatMessage(rec)
				+ (show_method_p ? "\t[" + rec.getSourceClassName() + "."
						+ rec.getSourceMethodName() + "]" : "") + "\n";
	}

}
