/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Strings {

	public static String trimQuotes(String str) {
		return Strings.trimQuotes(str, false);
	}

	public static String trimQuotes(String str, boolean space_p) {
		if (space_p)
			str = str.trim();
		str = str.replaceAll("^\"", "");
		str = str.replaceAll("\"$", "");
		if (space_p)
			str = str.trim();
		return str;
	}

	public static String toInitialCapital(String str) {
		if (str.length() <= 1)
			return str.toUpperCase();
		return str.substring(0, 1).toUpperCase()
				+ str.substring(1).toLowerCase();
	}

	public final static List<String> STOP_WORDS = Arrays.asList("i", "a", "am",
			"an", "are", "as", "at", "be", "being", "been", "by", "for",
			"from", "how", "in", "is", "it", "of", "on", "or", "that", "the",
			"this", "to", "was", "were", "what", "when", "where", "who",
			"will", "with");

	public static List<String> toLowerCase(List<String> words) {
		List<String> res = new ArrayList<String>();
		for (String w : words) {
			res.add(w.toLowerCase());
		}
		return res;
	}

	public static List<String> doWordTokenize(String str) {
		List<String> res = new ArrayList<String>();
		for (String w : str.split("\\W")) {
			if (!w.equals(""))
				res.add(w);
		}
		return res;
	}

	public static List<String> removeStopWords(List<String> words) {
		List<String> res = new ArrayList<String>();
		for (String w : words) {
			if (w.length() == 1)
				continue;
			if (STOP_WORDS.contains(w.toLowerCase()))
				continue;
			res.add(w);
		}
		return res;
	}

	public static float computeDiceCoefficient(Set<String> words1,
			Set<String> words2) {
		int i = 0;
		for (String w1 : words1) {
			if (words2.contains(w1))
				i++;
		}
		return (2.0f * i) / (words1.size() + words2.size());
	}

	public static Set<String> asWordSet(String str) {
		return new TreeSet<String>(
				removeStopWords(doWordTokenize(str.toLowerCase())));
	}

	public static float computeDiceCoeffiecientSimiliarity(String str1,
			String str2) {
		return computeDiceCoefficient(asWordSet(str1), asWordSet(str2));
	}

	public static float computeJaccardCoefficient(Set<String> words1,
			Set<String> words2) {
		/*
		 * |w1 int w2|/ (|w1| + |w2| - |w1 int w2|)
		 */
		int i = 0;
		for (String w1 : words1) {
			if (words2.contains(w1))
				i++;
		}
		return (float) i / (float) (words1.size() + words2.size() - i);
	}

}
