package com.mays.util;

import java.io.PrintWriter;
import java.util.Stack;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.mays.util.Html.Tag;

public class HtmlDocument extends HtmlDocumentDelegator {

	private Stack<Element> elements = new Stack<>();

	private Element head;

	private Element body;

	public HtmlDocument(Document document) {
		this.document = document;
	}

	public Element getRoot() {
		return document.getDocumentElement();
	}

	public Element getHead() {
		return head;
	}

	public Element getBody() {
		return body;
	}

	public Element peek() {
		return elements.peek();
	}

	public Element pop() {
		return elements.pop();
	}

	public static HtmlDocument newHtmlDocument() throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		Element root = doc.createElement("html");
		doc.appendChild(root);
		HtmlDocument html_document = new HtmlDocument(doc);
		html_document.init();
		return html_document;
	}

	protected void init() throws Exception {
		head = createElement(getRoot(), Tag.HEAD);
		body = createElement(getRoot(), Tag.BODY);
	}

	public void write(String file) throws Exception {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMSource source = new DOMSource(this);
		// appears to be the only way to get doctype into the file
		PrintWriter out = IO.createPrintUtf8FileWriter(file);
		out.println(Html.getDoctype());
		out.flush();
		StreamResult result = new StreamResult(out);
		// StreamResult result = new StreamResult(new File(file));
		// StreamResult result = new StreamResult(System.out);
		transformer.transform(source, result);
	}

	public Element createElement(Element parent, Tag name, String... attrs) throws Exception {
		return createTextElement(parent, name, null, attrs);
	}

	public Element createTextElement(Element parent, Tag name, String text, String... attrs) throws Exception {
		Document doc = parent.getOwnerDocument();
		Element elem = doc.createElement(name.toString());
		if (attrs.length % 2 != 0)
			throw new Exception("Attrs length: " + attrs.length);
		for (int i = 0; i < attrs.length; i = i + 2) {
			elem.setAttribute(attrs[i], attrs[i + 1]);
		}
		parent.appendChild(elem);
		if (text != null)
			addText(elem, text);
		elements.push(elem);
		return elem;
	}

//	public void addText(Element parent, String text) throws Exception {
//		addText(parent, text, true);
//	}

	public void addText(Element parent, String text) throws Exception {
		Document doc = parent.getOwnerDocument();
//		if (escape_p)
//			text = escapeValue(text);
		parent.appendChild(doc.createTextNode(text));
	}

//	public String escapeValue(String text) {
//		return Html.escapeValue(text);
//	}

}
