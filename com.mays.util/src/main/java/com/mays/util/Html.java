/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

public class Html {

	public enum Tag {
		HEAD, TITLE, LINK, SCRIPT,
		//
		BODY, A, B, BR, DIV, P,
		//
		TABLE, TH, TD, TR,
		//
		DEL, INS, HR, SPAN, UL, LI;
	}

	public static String getDoctype() {
		return "<!doctype html>";
	}

	public static String startElement(Tag name) {
		return startElement(name, null);
	}

	public static String startElement(Tag name, String[] attrs) {
		if (attrs != null)
			return Xml.startElement(name.toString().toLowerCase(), attrs);
		return Xml.startElement(name.toString().toLowerCase());
	}

	public static String endElement(Tag name) {
		return Xml.endElement(name.toString().toLowerCase());
	}

	public static String valueElement(Tag name, String[] attrs, String value) {
		return Html.startElement(name, attrs) + escapeValue(value) + Html.endElement(name);
	}

	public static String valueElement(Tag name, String value) {
		return Html.valueElement(name, null, value);
	}

	public static String escapeValue(String value) {
		if (value != null) {
			value = value.replace("&", "&#38;");
			value = value.replace("<", "&#60;");
			value = value.replace(">", "&#62;");
		}
		return value;
	}

}
