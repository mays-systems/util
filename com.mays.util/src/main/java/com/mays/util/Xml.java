/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Xml {

	public static String getDeclaration(String encoding) {
		// ISO-8859-1
		return "<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>";
	}

	public static String getStylesheet(String type, String href) {
		return "<?xml-stylesheet type=\"" + type + "\" href=\"" + href + "\"?>";
	}

	public static String startElement(String name) {
		return "<" + name + ">";
	}

	public static String startElement(String name, String[] attrs) {
		return startElement(name, attrs, false);
	}

	public static String startElement(String name, String[] attrs, boolean empty_p) {
		String res = "";
		res += "<";
		res += name + " ";
		int i = 0;
		for (String str : attrs) {
			if ((i % 2) == 0) {
				res += str + "=";
			} else {
				res += "\"" + str + "\" ";
			}
			i++;
		}
		if (empty_p)
			res += "/";
		res += ">";
		return res;
	}

	public static String emptyElement(String name) {
		return "<" + name + "/>";
	}

	public static String emptyElement(String name, String[] attrs) {
		return startElement(name, attrs, true);
	}

	public static String endElement(String name) {
		return "</" + name + ">";
	}

	public static String elementValue(String value) {
		return escapeValue(value);
	}

	public static String valueElement(String name, String value) {
		return startElement(name) + escapeValue(value) + endElement(name);
	}

	public static String valueElement(String name, long value) {
		return valueElement(name, String.valueOf(value));
	};

	public static String valueElement(String name, String[] attrs, String value) {
		return startElement(name, attrs) + escapeValue(value) + endElement(name);
	}

	public static String escapeValue(String value) {
		// This would be more robust
		// value = value.replace("&", "&#38;");
		// value = value.replace("<", "&#60;");
		if ((value.indexOf('&') >= 0) || (value.indexOf('>') >= 0) || (value.indexOf('<') >= 0))
			return "<![CDATA[" + value + "]]>";
		return value;
	}

	public static Iterable<Node> iterable(final NodeList n) {
		return new Iterable<Node>() {

			@Override
			public Iterator<Node> iterator() {

				return new Iterator<Node>() {

					int index = 0;

					@Override
					public boolean hasNext() {
						return index < n.getLength();
					}

					@Override
					public Node next() {
						if (hasNext()) {
							return n.item(index++);
						} else {
							throw new NoSuchElementException();
						}
					}

					@Override
					public void remove() {
						throw new UnsupportedOperationException();
					}
				};
			}
		};
	}

	public static String comment(String str) {
		return "<!-- " + str + " -->";
	}

}
