/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.util.logging.LogRecord;
import java.util.logging.StreamHandler;

/**
 * Similar to ConsoleHandler, but uses System.out rather than System.err. Would
 * be nice to extend ConsoleHandler instead of StreamHandler, but the method
 * setOutputStream actually closes the stream. We wouldn't want that to happen
 * to System.err since that would make all our errors go away.
 * 
 * @see java.util.logging.ConsoleHandler
 * @see StreamHandler
 * 
 * @author Eric Mays
 */
public class OutConsoleHandler extends StreamHandler {

	/**
	 * Create a ConsoleHandler for System.out.
	 */
	public OutConsoleHandler() {
		super();
		this.setOutputStream(System.out);
	}

	/**
	 * Just flush.
	 * 
	 * @see java.util.logging.StreamHandler#close()
	 */
	@Override
	public synchronized void close() throws SecurityException {
		super.flush();
	}

	/**
	 * Override this to flush immediately.
	 * 
	 * @see java.util.logging.StreamHandler#publish(java.util.logging.LogRecord)
	 */
	@Override
	public synchronized void publish(LogRecord rec) {
		super.publish(rec);
		super.flush();
	}

}