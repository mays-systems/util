/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.List;

/**
 * A few simple create instance methods for common file streams
 * 
 * @author Eric Mays
 * 
 */
public class IO {

	public static String NEWLINE = System.getProperty("line.separator");

	public static String getUserHomeDir() {
		return System.getProperty("user.home");
	}

	public static BufferedReader createBufferedFileReader(String file_name) throws FileNotFoundException {
		return new BufferedReader(new FileReader(file_name));
	}

	public static BufferedReader createBufferedUtf8FileReader(String file_name)
			throws FileNotFoundException, UnsupportedEncodingException {
		return new BufferedReader(new InputStreamReader(new FileInputStream(file_name), "UTF-8"));
	}

	public static PrintWriter createPrintFileWriter(String file_name) throws IOException {
		return new PrintWriter(new BufferedWriter(new FileWriter(file_name)));
	}

	public static PrintWriter createPrintUtf8FileWriter(String file_name) throws IOException {
		return new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file_name), "UTF-8")));
	}

	public static int countLines(String file_name) throws IOException {
		BufferedReader in = createBufferedFileReader(file_name);
		int line_i = 0;
		while (in.readLine() != null) {
			line_i++;
		}
		in.close();
		return line_i;
	}

	public static void copyFile(String in, String out) throws Exception {
		copyFile(new File(in), new File(out));
	}

	public static void copyFile(File in, File out) throws Exception {
		FileChannel in_c = new FileInputStream(in).getChannel();
		FileChannel out_c = new FileOutputStream(out).getChannel();
		out_c.transferFrom(in_c, 0, in_c.size());
		in_c.close();
		out_c.close();
	}

	public static void printlnTab(PrintWriter tab_out, String... values) throws Exception {
		IO.printlnTab(tab_out, Arrays.asList(values));
	}

	public static void printlnTab(PrintWriter tab_out, List<String> values) throws Exception {
		boolean first_p = true;
		for (String v : values) {
			if (!first_p)
				tab_out.print("\t");
			tab_out.print(v);
			first_p = false;
		}
		tab_out.println();
	}

}
