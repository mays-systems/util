/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Some convenience methods for doing JDBC.
 * 
 * @author Eric Mays
 * 
 */
public class Sql {

	public final static String DERBY_HOME_KEY = "derby_home";

	public final static String SCHEMA_KEY = "schema";

	public static Logger LOGGER;

	/**
	 * If the LOGGER has been set, use it, otherwise System.out with some added
	 * info lest we forget where this came from.
	 */
	private static void log(String msg) {
		if (LOGGER == null) {
			System.out.println(msg + "\t[" + Sql.class.getName() + ".?]");
		} else {
			LOGGER.warning(msg);
		}
	}

	public static Connection getConnection(String derby_home, String schema) throws Exception {
		return getConnection(derby_home, schema, false);
	}

	/**
	 * Establish a JDBC connection to an Apache Derby db, creating one if
	 * necessary, using the embedded driver.
	 * 
	 * @param derby_home
	 *            directory for schema
	 * @param schema
	 *            schema name
	 * @param create_p
	 *            if true create schema if it does not already exist
	 * @return Connection
	 * @throws Exception
	 *             exception
	 */
	public static Connection getConnection(String derby_home, String schema, boolean create_p) throws Exception {
		System.setProperty("derby.system.home", derby_home);
		Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
		Connection conn = DriverManager.getConnection("jdbc:derby:" + schema + (create_p ? ";create=true" : ""));
		conn.setAutoCommit(false);
		return conn;
	}

	/**
	 * Establish a JDBC connection to an Apache Derby db, creating one if
	 * necessary, using the embedded driver.
	 * 
	 * @param config
	 *            config
	 * @return Connection
	 * @throws Exception
	 *             exception
	 */
	public static Connection getConnection(Config config) throws Exception {
		return Sql.getConnection(config, null);
	}

	/**
	 * Establish a JDBC connection to an Apache Derby db, creating one if
	 * necessary, using the embedded driver.
	 * 
	 * @param config
	 *            config
	 * @param logger
	 *            logger
	 * @return Connection
	 * @throws Exception
	 *             exception
	 */
	public static Connection getConnection(Config config, Logger logger) throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, logger);
		String schema = config.getProperty(Sql.SCHEMA_KEY, logger);
		return Sql.getConnection(derby_home, schema);
	}

	public static Connection getConnection(String config_file, Logger logger) throws Exception {
		Config config = Config.load(config_file, logger);
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, logger);
		String schema = config.getProperty(Sql.SCHEMA_KEY, logger);
		return Sql.getConnection(derby_home, schema);
	}

	public static void shutdown(String schema) throws Exception {
		try {
			DriverManager.getConnection("jdbc:derby:" + schema + ";shutdown=true");
		} catch (SQLException ex) {
			log("Ignoring " + ex.getMessage());
		}
	}

	public static void restart() throws Exception {
		try {
			DriverManager.getConnection("jdbc:derby:" + "" + ";shutdown=true");
		} catch (SQLException ex) {
			log("Ignoring " + ex.getMessage());
		}
		System.gc();
	}

	/**
	 * Establish a JDBC connection to an ODBC db
	 * 
	 * Useful for extracting data from MS Access
	 * 
	 * @param dsn
	 *            ODBC dsn
	 * @return Connection
	 * @throws Exception
	 *             exception
	 */
	public static Connection getOdbcConnection(String dsn) throws Exception {
		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
		Connection conn = DriverManager.getConnection("jdbc:odbc:" + dsn);
		conn.setAutoCommit(false);
		return conn;
	}

	/**
	 * Drop the table, but catch the "does not exist error", log it, and
	 * proceed.
	 * 
	 * @param conn
	 *            connection
	 * @param name
	 *            table name
	 * @throws SQLException
	 *             exception
	 */
	public static void dropTable(Connection conn, String name) throws SQLException {
		String query = "DROP TABLE " + name;
		Statement stmt = conn.createStatement();
		try {
			stmt.executeUpdate(query);
		} catch (SQLException ex) {
			if (!ex.getSQLState().equals("42Y55")) {
				throw ex;
			}
			log("Ignoring " + ex.getMessage());
		} finally {
			stmt.close();
		}
	}

	public static void createTable(Connection conn, String name, String def) throws SQLException {
		Sql.createTable(conn, name, def, false);
	}

	/**
	 * Create a table
	 * 
	 * @param conn
	 *            connection
	 * @param name
	 *            table name
	 * @param def
	 *            table definition
	 * 
	 * @param drop_p
	 *            if true drop table
	 * 
	 * @throws SQLException
	 *             exception
	 */
	public static void createTable(Connection conn, String name, String def, boolean drop_p) throws SQLException {
		if (drop_p)
			Sql.dropTable(conn, name);
		String query = "CREATE TABLE " + name + " ( " + def + " )";
		Statement stmt = conn.createStatement();
		try {
			stmt.executeUpdate(query);
		} finally {
			stmt.close();
		}
	}

	public static void addColumn(Connection conn, String name, String def) throws SQLException {
		String query = "ALTER TABLE " + name + " ADD COLUMN " + def + " ";
		Statement stmt = conn.createStatement();
		try {
			stmt.executeUpdate(query);
		} finally {
			stmt.close();
		}
	}

	public static void deleteTable(Connection conn, String name) throws SQLException {
		String query = "DELETE FROM " + name;
		Statement stmt = conn.createStatement();
		try {
			stmt.executeUpdate(query);
		} finally {
			stmt.close();
		}
	}

	public static void deleteFromTable(Connection conn, String table, String key_column, String key_value)
			throws SQLException {
		String query = "";
		query += "DELETE FROM " + table + " ";
		query += " WHERE " + key_column + " = ? ";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, key_value);
		stmt.executeUpdate();
		stmt.close();
	}

	public static boolean doesTableExist(Connection conn, String name) throws SQLException {
		DatabaseMetaData md = conn.getMetaData();
		ResultSet res = md.getTables(null, null, name, null);
		return res.next();
	}

	/**
	 * Count the rows
	 * 
	 * @param conn
	 *            connection
	 * @param name
	 *            table name
	 * 
	 * @return The row count or -1 if something went wrong (without throwing an
	 *         Exception)
	 * 
	 * @throws SQLException
	 *             exception
	 */
	public static int countTable(Connection conn, String name) throws SQLException {
		int ret = -1;
		String query = "SELECT count(*) FROM " + name;
		Statement stmt = conn.createStatement();
		try {
			ResultSet res = stmt.executeQuery(query);
			res.next();
			ret = res.getInt(1);
			res.close();
		} finally {
			stmt.close();
		}
		return ret;
	}

	public static List<String> countTableValues(Connection conn, String table_name, String column_name)
			throws SQLException {
		ArrayList<String> ret = new ArrayList<String>();
		String query = "";
		query += "SELECT " + column_name + ", count(*) ";
		query += "  FROM " + table_name;
		query += " GROUP BY " + column_name;
		Statement stmt = conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		while (res.next()) {
			ret.add(res.getString(1) + "\t" + res.getInt(2));
		}
		res.close();
		stmt.close();
		return ret;
	}

	/**
	 * Create the index using the table and column names for the index name
	 * 
	 * @param conn
	 *            connection
	 * @param table
	 *            table name
	 * @param column
	 *            column to index
	 * @throws SQLException
	 *             exception
	 */
	public static void createIndex(Connection conn, String table, String column) throws SQLException {
		String index_name = column;
		index_name = index_name.replace(" ", "_");
		index_name = index_name.replace(",", "_");
		String query = "CREATE INDEX " + table + "__" + index_name + " ON " + table + " ( " + column + " )";
		Statement stmt = conn.createStatement();
		try {
			stmt.executeUpdate(query);
		} finally {
			stmt.close();
		}
	}

	/**
	 * Set an int parameter on a PreparedStatement from a String, doing the
	 * right thing if the string is empty
	 * 
	 * @param stmt
	 *            statement
	 * @param parameter_i
	 *            statement parameter index
	 * @param value
	 *            may be null
	 * @throws SQLException
	 *             exception
	 */
	public static void setIntOrNull(PreparedStatement stmt, int parameter_i, String value) throws SQLException {
		if (value.equals("")) {
			stmt.setNull(parameter_i, Types.INTEGER);
		} else {
			stmt.setInt(parameter_i, Integer.parseInt(value));
		}
	}

	public static void exportTable(Connection conn, String table_name, String file_name, Logger logger)
			throws Exception {
		PrintWriter out = IO.createPrintUtf8FileWriter(file_name);
		String query;
		query = "";
		query += "SELECT * FROM " + table_name;
		Statement stmt = conn.createStatement();
		ResultSet res = stmt.executeQuery(query);
		ResultSetMetaData resmd = res.getMetaData();
		int cols = resmd.getColumnCount();
		for (int i = 1; i <= cols; i++) {
			out.print(resmd.getColumnName(i));
			if (i != cols) {
				out.print("\t");
			}
		}
		out.println();
		int count = 0;
		while (res.next()) {
			count++;
			for (int i = 1; i <= cols; i++) {
				out.print(res.getObject(i));
				if (i != cols) {
					out.print("\t");
				}
			}
			out.println();
		}
		res.close();
		stmt.close();
		out.close();
		logger.info("Processed " + count);
	}

	public static String getInsertValues(String columns) {
		String ret = "?";
		for (char c : columns.toCharArray()) {
			if (c == ',') {
				ret += ",?";
			}
		}
		return ret;
	}

	public static void setInsertValues(PreparedStatement stmt, List<Object> values) throws SQLException {
		int i = 0;
		for (Object o : values) {
			i++;
			stmt.setObject(i, o);
		}
	}

	public void copyTable(Connection conn1, String table1, Connection conn2, String table2) throws SQLException {
		String query;
		query = "";
		query += "SELECT * ";
		query += "  FROM " + table1 + " ";
		Statement stmt = conn1.createStatement();
		ResultSet res = stmt.executeQuery(query);
		ResultSetMetaData resmd = res.getMetaData();
		String table_def = "";
		String insert_cols = "";
		String insert_args = "";
		for (int i = 1; i <= resmd.getColumnCount(); i++) {
			BasicLogger.trace(resmd.getColumnName(i) + "\t" + resmd.getColumnType(i) + "\t" + resmd.getColumnTypeName(i)
					+ "\t" + resmd.getPrecision(i));
			String column_name = resmd.getColumnName(i);
			String column_def = column_name + " " + resmd.getColumnTypeName(i);
			switch (resmd.getColumnType(i)) {
			case Types.VARCHAR:
				column_def += " (" + resmd.getPrecision(i) + ") ";
				break;
			case Types.CHAR:
				column_def += " (" + resmd.getPrecision(i) + ") ";
				break;
			}
			table_def += column_def;
			insert_cols += column_name;
			insert_args += "?";
			if (i != resmd.getColumnCount()) {
				table_def += ", \n";
				insert_cols += ", ";
				insert_args += ", ";
			}
		}
		LOGGER.info("Table def:\n" + table_def);
		Sql.dropTable(conn2, table2);
		Sql.createTable(conn2, table2, table_def);
		query = "";
		query += "INSERT INTO " + table2 + " ";
		query += "( " + insert_cols + " ) ";
		query += "VALUES ( " + insert_args + " ) ";
		PreparedStatement stmt_i = conn2.prepareStatement(query);
		int rec_i = 0;
		while (res.next()) {
			rec_i++;
			for (int i = 1; i <= resmd.getColumnCount(); i++) {
				switch (resmd.getColumnType(i)) {
				case Types.INTEGER:
					stmt_i.setInt(i, res.getInt(i));
					break;
				case Types.VARCHAR:
					stmt_i.setString(i, res.getString(i));
					break;
				case Types.CHAR:
					stmt_i.setString(i, res.getString(i));
					break;
				default:
					LOGGER.warning("Column type not handled");
				}
			}
			stmt_i.executeUpdate();
		}
		LOGGER.info("Record count: " + rec_i);
		res.close();
		stmt.close();
		stmt_i.close();
		conn2.commit();
		LOGGER.info("Table1 count: " + Sql.countTable(conn1, table1));
		LOGGER.info("Table2 count: " + Sql.countTable(conn2, table2));
	}

}
