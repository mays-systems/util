/**
 * Copyright 2008-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A simple logger utility which sets up logging to a file and the console
 * 
 * The file uses the name of the application class
 * 
 * @author Eric Mays
 * 
 */
public class BasicLogger extends Logger {

	protected BasicLogger(String name, String resourceBundleName) {
		super(name, resourceBundleName);
	}

	public static Logger init(Class<?> application_class) throws IOException {
		return init(application_class, true);
	}

	public static Logger init(Class<?> application_class, boolean show_method_p) throws IOException {
		return init(application_class, show_method_p, false);
	}

	public static Logger init(Class<?> application_class, boolean show_method_p, boolean use_log_dir)
			throws IOException {
		return init(application_class, "", show_method_p, use_log_dir);
	}

	public static Logger init(Class<?> application_class, String tag, boolean show_method_p, boolean use_log_dir)
			throws IOException {
		String log_dir = "";
		if (use_log_dir)
			log_dir = "logs/";
		return init(application_class, tag, show_method_p, log_dir);
	}

	public static String getLogDir(String[] args) {
		int i = 0;
		for (String arg : args) {
			if (arg.equals("-logDir"))
				return args[i + 1];
			i++;
		}
		return null;
	}

	public static Logger init(Class<?> application_class, boolean show_method_p, String log_dir) throws IOException {
		return init(application_class, "", show_method_p, log_dir);
	}

	public static Logger init(Class<?> application_class, String tag, boolean show_method_p, String log_dir)
			throws IOException {
		if (log_dir != null && !log_dir.isEmpty())
			Files.createDirectories(Paths.get(log_dir));
		return init(application_class, log_dir + application_class.getName() + tag + ".log", show_method_p);
	}

	public static Logger init(Class<?> application_class, String log_file_name, boolean show_method_p)
			throws IOException {
		Logger logger = Logger.getLogger(application_class.getName());
		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);
		OutConsoleHandler console = new OutConsoleHandler();
		console.setLevel(Level.ALL);
		console.setFormatter(new SingleLineFormatter(show_method_p));
		logger.addHandler(console);
		FileHandler log_file = new FileHandler(log_file_name, false);
		log_file.setLevel(Level.CONFIG);
		log_file.setFormatter(new SingleLineFormatter(show_method_p));
		logger.addHandler(log_file);
		logger.info("START");
		logger.info("Date: " + new Date());
		logger.info(logger.getName());
		console.flush();
		return logger;
	}

	public static Logger createConsoleLogger() throws IOException {
		Logger logger = Logger.getLogger(BasicLogger.class.getName());
		logger.setLevel(Level.ALL);
		logger.setUseParentHandlers(false);
		OutConsoleHandler console = new OutConsoleHandler();
		console.setLevel(Level.ALL);
		console.setFormatter(new SingleLineFormatter(false));
		logger.addHandler(console);
		// logger.info("START");
		// logger.info("Date: " + new Date());
		// logger.info(logger.getName());
		console.flush();
		return logger;
	}

	public static void log(Throwable ex, Logger logger) {
		System.out.println(ex.getMessage());
		System.out.flush();
		ex.printStackTrace();
		System.out.flush();
		System.err.flush();
		if (logger != null) {
			logger.severe(ex.getMessage());
			logger.severe(ex.toString());
			for (StackTraceElement ste : ex.getStackTrace()) {
				logger.severe("\t" + ste);
			}
		} else {
			System.err.println("NULL LOGGER");
			System.err.flush();
		}
	}

	public static boolean TRACE_P = false;

	public static void trace(Object msg) {
		if (!TRACE_P)
			return;
		StackTraceElement[] st = Thread.currentThread().getStackTrace();
		System.out.println(
				"@" + st[2].getClassName() + "." + st[2].getMethodName() + ":" + st[2].getLineNumber() + ">>>" + msg);
		System.out.flush();
	}

	public static void end(Logger logger) {
		logger.info("END");
		for (Handler h : logger.getHandlers()) {
			logger.removeHandler(h);
			h.close();
		}
	}

}
