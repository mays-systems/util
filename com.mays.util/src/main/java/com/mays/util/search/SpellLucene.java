/**
 * Copyright 2007-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeSet;

import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.spell.Dictionary;
import org.apache.lucene.search.spell.PlainTextDictionary;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.store.FSDirectory;

import com.mays.util.IO;

public class SpellLucene {

	private String dir;

	private SpellChecker checker;

	private HashSet<String> short_words;

	private HashSet<String> custom_words;

	private boolean acronym_words = false;

	public SpellLucene(String dir) throws Exception {
		this.dir = dir;
	}

	public void init() throws Exception {
		File file = new File(dir + "/spell_index");
		if (!file.exists()) {
			IO.createPrintUtf8FileWriter(dir + "/short_words.txt").close();
			IO.createPrintUtf8FileWriter(dir + "/custom_words.txt").close();
		}
		this.checker = new SpellChecker(FSDirectory.open(file.toPath()));
		this.short_words = new HashSet<String>();
		{
			BufferedReader in = IO.createBufferedUtf8FileReader(dir + "/short_words.txt");
			String line;
			while ((line = in.readLine()) != null) {
				short_words.add(line);
			}
		}
		this.custom_words = new HashSet<String>();
		{
			BufferedReader in = IO.createBufferedUtf8FileReader(dir + "/custom_words.txt");
			String line;
			while ((line = in.readLine()) != null) {
				custom_words.add(line);
			}
		}
	}

	public String getImportLog() throws Exception {
		File log_file = new File(dir + "/import_log.txt");
		if (!log_file.exists())
			return "";
		String ret = "";
		BufferedReader in = IO.createBufferedUtf8FileReader(dir + "/import_log.txt");
		String line;
		while ((line = in.readLine()) != null) {
			ret += line + "\n";
		}
		return ret;
	}

	public void addWords(String file) throws Exception {
		File log_file = new File(dir + "/import_log.txt");
		addWords(file, !log_file.exists());
	}

	private void addWords(String file, boolean first_p) throws Exception {
		Dictionary dictionary = new PlainTextDictionary(new File(file).toPath());
		IndexWriterConfig iwc = new IndexWriterConfig(new SpellingAnalyzer());
		iwc.setOpenMode((first_p ? OpenMode.CREATE : OpenMode.APPEND));
		// System.out.println(iwc.getOpenMode());
		this.checker.indexDictionary(dictionary, iwc, true);
		BufferedReader in = IO.createBufferedUtf8FileReader(file);
		String line;
		while ((line = in.readLine()) != null) {
			if (line.length() < 3)
				short_words.add(line);
		}
		{
			PrintWriter out = IO.createPrintUtf8FileWriter(dir + "/short_words.txt");
			ArrayList<String> words = new ArrayList<String>(short_words);
			Collections.sort(words);
			for (String w : words) {
				out.println(w);
			}
			out.close();
		}
		{
			String str = getImportLog();
			PrintWriter out = IO.createPrintUtf8FileWriter(dir + "/import_log.txt");
			out.print(str);
			out.println(file);
			out.close();
		}
		init();
	}

	public boolean isWord(String word) {
		if (custom_words.contains(word))
			return true;
		if (this.acronym_words && word.toUpperCase().equals(word))
			return true;
		if (word.length() < 3)
			return short_words.contains(word);
		try {
			if (checker.exist(word))
				return true;
			if (word.matches(".*[\\-/].*")) {
				boolean found_all = true;
				for (String w : word.split("[\\-/]")) {
					if (!isWord(w)) {
						found_all = false;
						break;
					}
				}
				if (found_all)
					return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public String[] suggest(String word) {
		try {
			return checker.suggestSimilar(word, 5);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String[0];
	}

	public String[] getCustomWords() {
		TreeSet<String> words = new TreeSet<String>(custom_words);
		return words.toArray(new String[0]);
	}

	public void setCustomWords(String[] words) {
		PrintWriter out;
		try {
			out = IO.createPrintUtf8FileWriter(dir + "/custom_words.txt");
			for (String w : new TreeSet<String>(Arrays.asList(words))) {
				out.println(w);
			}
			out.close();
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean getAcronymWords() {
		return acronym_words;
	}

	public void setAcronymWords(boolean aw) {
		this.acronym_words = aw;
	}

}
