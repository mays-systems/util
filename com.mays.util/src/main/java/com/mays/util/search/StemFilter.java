/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.io.IOException;
import java.util.HashMap;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class StemFilter extends TokenFilter {

	private PorterStemmer stemmer;

	private CharTermAttribute term_attr;

	private HashMap<String, String> stems;

	public StemFilter(TokenStream in) {
		super(in);
		stemmer = new PorterStemmer();
		term_attr = addAttribute(CharTermAttribute.class);
		stems = new HashMap<String, String>();
		stems.put("conjunctiva", "conjunctiva");
		stems.put("conjunctival", "conjunctiva");
		stems.put("cornea", "cornea");
		stems.put("corneal", "cornea");
		stems.put("retina", "retina");
		stems.put("retinal", "retina");
		stems.put("macula", "macula");
		stems.put("macular", "macula");
		stems.put("eye", "eye");
		stems.put("eyes", "eye");
		// stems.put("ocular", "eye");
		// stems.put("ophthalmic", "eye");
		stems.put("teeth", "tooth");
		stems.put("tooth", "tooth");
	}

	// public final Token next(final Token reusableToken) throws IOException {
	// assert reusableToken != null;
	// Token nextToken = input.next(reusableToken);
	// if (nextToken == null)
	// return null;
	// String token_str = nextToken.termText();
	// String stem = stems.get(token_str);
	// if (stem != null) {
	// nextToken.setTermBuffer(stem.toCharArray(), 0, stem.length());
	// return nextToken;
	// }
	// if (stemmer.stem(nextToken.termBuffer(), 0, nextToken.termLength()))
	// nextToken.setTermBuffer(stemmer.getResultBuffer(), 0,
	// stemmer.getResultLength());
	// return nextToken;
	// }

	@Override
	public final boolean incrementToken() throws IOException {
		if (!input.incrementToken())
			return false;

		String stem = stems.get(term_attr.toString());
		if (stem != null) {
			// term_attr.setTermBuffer(stem);
			term_attr.setEmpty();
			term_attr.append(stem);
			return true;
		}

		if (stemmer.stem(term_attr.buffer(), 0, term_attr.length()))
			term_attr.copyBuffer(stemmer.getResultBuffer(), 0,
					stemmer.getResultLength());
		return true;
	}

}
