/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.NumericDocValuesField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.FSDirectory;

import com.mays.util.BasicProgressMonitor;
import com.mays.util.BasicProgressMonitorNoOp;
import com.mays.util.Config;
import com.mays.util.IO;
import com.mays.util.Sql;

public abstract class BuildLuceneIndexBase {

	public static final String WORD_SPLIT = "\\W";

	public static Logger LOGGER;

	protected Config config;

	protected Connection conn;

	protected BasicProgressMonitor progress_monitor;

	protected IndexWriter writer;

	protected HashMap<String, Integer> words;

	protected Set<Long> index_roots;

	public BuildLuceneIndexBase(String config_name) throws Exception {
		super();
		this.config = Config.load(config_name, LOGGER);
	}

	public BuildLuceneIndexBase(Config config) throws Exception {
		super();
		this.config = config;
	}

	public void setProgressMonitor(BasicProgressMonitor progress_monitor) {
		this.progress_monitor = progress_monitor;
	}

	public void init() throws Exception {
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		this.init(Sql.getConnection(derby_home, schema));
	}

	public void init(Connection conn) throws Exception {
		this.conn = conn;
	}

	public void shutdown() throws Exception {
		conn.rollback();
		conn.close();
		Sql.shutdown(config.getProperty(Sql.SCHEMA_KEY, LOGGER));
	}

	public Set<Long> getIndexRoots() {
		return index_roots;
	}

	public void setIndexRoots(Set<Long> index_roots) {
		this.index_roots = index_roots;
	}

	public abstract void setIndexRoots() throws Exception;

	protected abstract boolean indexRootP(long root_id, long concept_id) throws Exception;

	public void indexTerm(String term, long concept_id) throws Exception {
		Document doc = new Document();
		doc.add(new TextField("contents", term, Field.Store.YES));
		doc.add(new StringField("id", String.valueOf(concept_id), Field.Store.YES));
		// Think about search by id
		for (long root_id : index_roots) {
			if (indexRootP(root_id, concept_id))
				doc.add(new StringField("root", String.valueOf(root_id), Field.Store.YES));
		}
		writer.addDocument(doc);
		for (String word : term.split(WORD_SPLIT)) {
			word = word.toLowerCase();
			Integer freq = words.get(word);
			if (freq == null)
				freq = 0;
			freq++;
			words.put(word, freq);
		}
	}

	protected abstract void build() throws Exception;

	public void run() throws Exception {
		SearchLucene.LOGGER = LOGGER;
		if (progress_monitor == null)
			progress_monitor = new BasicProgressMonitorNoOp();
		this.init();
		String lucene_home = SearchLucene.getLuceneHome(config);
		Analyzer analyzer = new SnomedAnalyzer();
		((SnomedAnalyzer) analyzer).setIndexing(true);
		IndexWriterConfig iw_config = new IndexWriterConfig(analyzer);
		iw_config.setOpenMode(OpenMode.CREATE);
		// IndexWriter.MaxFieldLength.LIMITED
		FSDirectory fsd = FSDirectory.open(new File(lucene_home).toPath());
		writer = new IndexWriter(fsd, iw_config);
		// writer.setUseCompoundFile(false);
		words = new HashMap<String, Integer>();
		setIndexRoots();
		String roots_file = SearchLucene.getLuceneIndexRoots(config);
		PrintWriter out_roots = IO.createPrintUtf8FileWriter(roots_file);
		for (long root : getIndexRoots()) {
			out_roots.println(root);
		}
		out_roots.close();
		this.build();
		LOGGER.info("Indexed: " + writer.maxDoc());
		int line_i = 0;
		for (Entry<String, Integer> word_freq : words.entrySet()) {
			line_i++;
			Document doc = new Document();
			doc.add(new StringField("word", word_freq.getKey(), Field.Store.YES));
			doc.add(new NumericDocValuesField("freq", word_freq.getValue()));
			writer.addDocument(doc);
			if (line_i % 10000 == 0) {
				LOGGER.fine("Line: " + line_i);
				progress_monitor.subTask("Search index words " + line_i);
			}
		}
		LOGGER.info("Indexed: " + writer.maxDoc());
		// writer.optimize();
		writer.close();
		fsd.close();
		analyzer.close();
		this.shutdown();
	}

}
