/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;

public class SpellingAnalyzer extends Analyzer {

	// public TokenStream tokenStream(String fieldName, Reader reader) {
	// StandardTokenizer tokenStream = new StandardTokenizer(
	// SearchLucene.LUCENE_VERSION, reader);
	// // tokenStream.setMaxTokenLength(maxTokenLength);
	// TokenStream result = new StandardFilter(SearchLucene.LUCENE_VERSION,
	// tokenStream);
	// return result;
	// }

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		StandardTokenizer source = new StandardTokenizer();
		// tokenStream.setMaxTokenLength(maxTokenLength);
		TokenStream filter = new StandardFilter(source);
		return new TokenStreamComponents(source, filter);
	}

}
