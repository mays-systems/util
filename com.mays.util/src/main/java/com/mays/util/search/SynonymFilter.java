/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

public class SynonymFilter extends TokenFilter {

	public static final String TOKEN_TYPE_SYNONYM = "SYNONYM";

	private CharTermAttribute term_attr;

	private PositionIncrementAttribute pos_incr_attr;

	private HashMap<String, Set<String>> synonyms;

	private Stack<String> synonym_stack;

	public SynonymFilter(TokenStream in) {
		super(in);
		synonyms = new HashMap<String, Set<String>>();
		addSynonym("lid", "eyelid");
		addSynonym("lymphadenopathy", "adenopathy");
		synonym_stack = new Stack<String>();
		term_attr = addAttribute(CharTermAttribute.class);
		pos_incr_attr = addAttribute(PositionIncrementAttribute.class);
	}

	private void addSynonym(String word, String synonym) {
		addSynonym1(word, synonym);
		addSynonym1(synonym, word);
	}

	private void addSynonym1(String word, String synonym) {
		Set<String> syns = this.synonyms.get(word);
		if (syns == null) {
			syns = new HashSet<String>();
			this.synonyms.put(word, syns);
		}
		syns.add(synonym);
	}

	// @Override
	// public Token next() throws IOException {
	// if (synonym_stack.size() > 0)
	// return synonym_stack.pop();
	// Token token = input.next();
	// if (token == null)
	// return null;
	// Set<String> syns = this.synonyms.get(token.termText());
	// if (syns != null) {
	// for (String syn : syns) {
	// // System.out.println("Syn " + token.termText() + " " + syn);
	// Token syn_token = new Token(syn, token.startOffset(),
	// token.endOffset(), TOKEN_TYPE_SYNONYM);
	// syn_token.setPositionIncrement(0);
	// synonym_stack.push(syn_token);
	// }
	// }
	// return token;
	// }

	@Override
	public final boolean incrementToken() throws IOException {
		if (synonym_stack.size() > 0) {
			// term_attr.setTermBuffer(synonym_stack.pop());
			term_attr.setEmpty();
			term_attr.append(synonym_stack.pop());
			pos_incr_attr.setPositionIncrement(0);
			return true;
		}

		if (!input.incrementToken())
			return false;

		Set<String> syns = this.synonyms.get(term_attr.toString());
		if (syns != null) {
			for (String syn : syns) {
				synonym_stack.push(syn);
			}
		}

		return true;
	}

}
