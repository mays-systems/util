/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

public class SearchResult {

	private String term;

	private long concept_id;

	public long getConceptId() {
		return concept_id;
	}

	public String getTerm() {
		return term;
	}

	public SearchResult(String term, long concept_id) {
		super();
		this.term = term;
		this.concept_id = concept_id;
	}

	public String toString() {
		return this.getTerm();
	}

}
