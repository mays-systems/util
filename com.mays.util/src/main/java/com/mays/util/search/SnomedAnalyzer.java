/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.util.Arrays;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;

public class SnomedAnalyzer extends Analyzer {

	public static final List<String> STOP_WORDS = Arrays.asList("a", "an",
			"and", "are", "as", "at", "be", "been", "being", "by", "came",
			"can", "did", "do", "does", "for", "from", "has", "had", "have",
			"here", "how", "if", "in", "is", "it", "its", "of", "on", "or",
			"that", "the", "was", "were",
			//
			"nos");

	boolean indexing = false;

	public void setIndexing(boolean indexing) {
		this.indexing = indexing;
	}

	// public TokenStream tokenStream(String fieldName, Reader reader) {
	// StandardTokenizer tokenStream = new StandardTokenizer(
	// SearchLucene.LUCENE_VERSION, reader);
	// // tokenStream.setMaxTokenLength(maxTokenLength);
	// TokenStream result = new StandardFilter(tokenStream);
	// result = new LowerCaseFilter(result);
	// result = new StopFilter(false, result, new HashSet<String>(STOP_WORDS));
	// // TODO implement a decent synonyms list
	// // if (indexing)
	// // result = new SynonymFilter(result);
	// result = new StemFilter(result);
	// return result;
	// }

	@Override
	protected TokenStreamComponents createComponents(String fieldName) {
		StandardTokenizer source = new StandardTokenizer();
		// tokenStream.setMaxTokenLength(maxTokenLength);
		TokenStream filter = new StandardFilter(source);
		filter = new LowerCaseFilter(filter);
		filter = new StopFilter(filter, new CharArraySet(STOP_WORDS, true));
		// TODO implement a decent synonyms list
		// if (indexing)
		// result = new SynonymFilter(result);
		filter = new StemFilter(filter);
		return new TokenStreamComponents(source, filter);
	}

}
