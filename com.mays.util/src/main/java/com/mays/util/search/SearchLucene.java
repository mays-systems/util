/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util.search;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.BooleanQuery.Builder;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.mays.util.Config;
import com.mays.util.Sql;

public class SearchLucene {

	public static final Version LUCENE_VERSION = Version.LUCENE_5_3_1;

	public static final String WORD_SPLIT = "\\W";

	public final static String LUCENE_HOME_KEY = "lucene_home";

	public static final Set<Long> EMPTY_CONS = Collections.emptySet();

	public static Logger LOGGER;

	// private Connection conn;

	private IndexSearcher isearcher;

	private FSDirectory directory;

	private DirectoryReader reader;

	private Analyzer analyzer;

	public SearchLucene() {
		super();
		// this.conn = conn;
	}

	public void init(String lucene_home) throws Exception {
		this.analyzer = new SnomedAnalyzer();
		directory = FSDirectory.open(new File(lucene_home).toPath());
		reader = DirectoryReader.open(directory);
		this.isearcher = new IndexSearcher(reader);
	}

	public void shutdown() throws Exception {
		// this.isearcher.close();
		this.reader.close();
		this.directory.close();
	}

	public static String getLuceneIndex(String derby_home, String schema) {
		return derby_home + "/" + schema + "_lucene_index/";
	}

	public static String getLuceneHome(Config config) {
		String lucene_home = config.getProperty(SearchLucene.LUCENE_HOME_KEY, LOGGER);
		if (lucene_home != null)
			return lucene_home;
		if (LOGGER != null)
			LOGGER.warning(SearchLucene.LUCENE_HOME_KEY + " not found");
		String derby_home = config.getProperty(Sql.DERBY_HOME_KEY, LOGGER);
		String schema = config.getProperty(Sql.SCHEMA_KEY, LOGGER);
		return getLuceneIndex(derby_home, schema);
	}

	public static String getLuceneIndexRoots(Config config) {
		return getLuceneIndexRoots(getLuceneHome(config));
	}

	public static String getLuceneIndexRoots(String lucene_home) {
		String roots_file = lucene_home;
		roots_file = roots_file.replaceAll("/+$", "");
		roots_file += "_roots.txt";
		if (LOGGER != null)
			LOGGER.info("Lucene roots file: " + roots_file);
		return roots_file;
	}

	public ArrayList<String> getTokens(String term) throws Exception {
		ArrayList<String> ret = new ArrayList<String>();
		TokenStream ts = this.analyzer.tokenStream("contents", new StringReader(term));
		CharTermAttribute term_attr = ts.addAttribute(CharTermAttribute.class);
		ts.reset();
		while (ts.incrementToken()) {
			ret.add(term_attr.toString());
		}
		ts.end();
		ts.close();
		return ret;
	}

	public String getTokenLine(String term, String delim) throws Exception {
		List<String> tokens = this.getTokens(term);
		String ret = "";
		for (String str : tokens) {
			ret += str + delim;
		}
		return ret;
	}

	public ArrayList<Document> search(String term, int page, int page_size) throws Exception {
		Set<Long> cons = Collections.emptySet();
		return this.search(term, page, page_size, cons, cons);
	}

	// This doesn't belong here
	private SortedSet<String> suggest_words_search;

	private BooleanQuery getQuery(String term) throws Exception {
		Set<Long> cons = Collections.emptySet();
		return getQuery(term, cons, cons);
	}

	private BooleanQuery getQuery(String term, Collection<Long> includes, Collection<Long> excludes) throws Exception {
		Builder builder = new BooleanQuery.Builder();
		for (String token : getTokens(term)) {
			builder.add(new TermQuery(new Term("contents", token)), BooleanClause.Occur.SHOULD);
		}
		Builder includes_builder = null;
		for (long id : includes) {
			if (includes_builder == null)
				includes_builder = new BooleanQuery.Builder();
			includes_builder.add(new TermQuery(new Term("root", String.valueOf(id))), BooleanClause.Occur.SHOULD);
		}
		if (includes_builder != null) {
			includes_builder.setMinimumNumberShouldMatch(1);
			builder.add(includes_builder.build(), BooleanClause.Occur.MUST);
		}
		for (long id : excludes) {
			builder.add(new TermQuery(new Term("root", String.valueOf(id))), BooleanClause.Occur.MUST_NOT);
		}
		return builder.build();
	}

	public ArrayList<Document> search(String term, int page, int page_size, Collection<Long> includes,
			Collection<Long> excludes) throws Exception {
		ArrayList<Document> ret = new ArrayList<Document>();
		// BooleanQuery query = new BooleanQuery();
		// for (String token : getTokens(term)) {
		// query.add(new TermQuery(new Term("contents", token)),
		// BooleanClause.Occur.SHOULD);
		// }
		// BooleanQuery includes_query = null;
		// for (long id : includes) {
		// if (includes_query == null)
		// includes_query = new BooleanQuery();
		// includes_query.add(
		// new TermQuery(new Term("root", String.valueOf(id))),
		// BooleanClause.Occur.SHOULD);
		// }
		// if (includes_query != null) {
		// includes_query.setMinimumNumberShouldMatch(1);
		// query.add(includes_query, BooleanClause.Occur.MUST);
		// }
		// for (long id : excludes) {
		// query.add(new TermQuery(new Term("root", String.valueOf(id))),
		// BooleanClause.Occur.MUST_NOT);
		// }
		BooleanQuery query = getQuery(term, includes, excludes);
		int top_n = page * page_size;
		TopDocs hits = isearcher.search(query, top_n);
		if (page <= 0)
			throw new Exception("page <= 0: " + page);
		for (int i = (page - 1) * page_size; i < top_n; i++) {
			if (i >= hits.totalHits)
				break;
			if (i >= top_n)
				break;
			ret.add(isearcher.doc(hits.scoreDocs[i].doc));
		}
		// This doesn't belong here
		if (page == 1) {
			suggest_words_search = new TreeSet<String>();
			hits = isearcher.search(query, 100);
			for (ScoreDoc d : hits.scoreDocs) {
				for (String word : isearcher.doc(d.doc).get("contents").split(WORD_SPLIT)) {
					suggest_words_search.add(word.toLowerCase());
				}
			}
		}
		return ret;
	}

	public ArrayList<Long> searchConcepts(String term, int page, int page_size, Collection<Long> includes,
			Collection<Long> excludes) throws Exception {
		ArrayList<Long> ret = new ArrayList<Long>();
		for (Document doc : this.search(term, page, page_size, includes, excludes)) {
			String id = doc.getField("id").stringValue();
			ret.add(Long.parseLong(id));
		}
		return ret;
	}

	public ArrayList<SearchResult> searchTerms(String sub_term, int page, int page_size) throws Exception {
		Set<Long> cons = Collections.emptySet();
		return searchTerms(sub_term, page, page_size, cons, cons);
	}

	public ArrayList<SearchResult> searchTerms(String sub_term, int page, int page_size, Collection<Long> includes,
			Collection<Long> excludes) throws Exception {
		ArrayList<SearchResult> ret = new ArrayList<SearchResult>();
		for (Document doc : search(sub_term, page, page_size, includes, excludes)) {
			String id = doc.getField("id").stringValue();
			String term = doc.getField("contents").stringValue();
			ret.add(new SearchResult(term, Long.parseLong(id)));
			// BasicLogger.trace(sub_term + " " + page + " " + page_size + " "
			// + id + " " + term);
		}
		return ret;
	}

	public Document findExactMatch(String term, List<Document> docs) throws Exception {
		Set<String> term_toks = new TreeSet<String>(this.getTokens(term));
		for (Document doc : docs) {
			Set<String> doc_toks = new TreeSet<String>(this.getTokens(doc.getField("contents").stringValue()));
			if (doc_toks.equals(term_toks)) {
				return doc;
			}
		}
		return null;
	}

	@Deprecated
	public ArrayList<String> suggestWords(String sub_word, int limit, boolean use_context) {
		ArrayList<String> ret = new ArrayList<String>();
		// BasicLogger.trace(sub_word + " " + limit);
		int mcc = BooleanQuery.getMaxClauseCount();
		try {
			if (use_context) {
				for (String word : suggest_words_search) {
					if (word.startsWith(sub_word))
						ret.add(word);
					if (ret.size() == limit)
						break;
				}
			}
			if (ret.size() < limit) {
				BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);
				TopDocs hits = isearcher.search(new WildcardQuery(new Term("word", sub_word + "*")), limit,
						new Sort(new SortField("freq", SortField.Type.LONG, true)));
				for (ScoreDoc sd : hits.scoreDocs) {
					String word = isearcher.doc(sd.doc).getField("word").stringValue();
					ret.add(word);
				}
			}
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} finally {
			BooleanQuery.setMaxClauseCount(mcc);
		}
		return ret;
	}

	public ArrayList<String> suggestWords(String sub_term, int limit) {
		ArrayList<String> ret = new ArrayList<String>();
		// BasicLogger.trace(sub_word + " " + limit);
		int mcc = BooleanQuery.getMaxClauseCount();
		try {
			if (sub_term.length() == 0)
				return ret;
			String sub_word = "";
			if (!sub_term.endsWith(" "))
				sub_word = sub_term.substring(sub_term.lastIndexOf(" ") + 1);
			if (sub_term.contains(" ")) {
				String term = sub_term.substring(0, sub_term.lastIndexOf(" ") - 1);
				// System.out.println(">ST:" + sub_term + ">SW:" + sub_word);
				TreeSet<String> suggest_words = new TreeSet<String>();
				BooleanQuery query = getQuery(term);
				TopDocs hits = isearcher.search(query, 100);
				for (ScoreDoc d : hits.scoreDocs) {
					for (String word : isearcher.doc(d.doc).get("contents").split(WORD_SPLIT)) {
						suggest_words.add(word.toLowerCase());
					}
				}
				for (String word : suggest_words) {
					if (word.startsWith(sub_word))
						ret.add(word);
					if (ret.size() == limit)
						break;
				}
			}
			if (ret.size() < limit) {
				// System.out.println(">ST:" + sub_term + ">");
				BooleanQuery.setMaxClauseCount(Integer.MAX_VALUE);
				TopDocs hits = isearcher.search(new WildcardQuery(new Term("word", sub_word + "*")), null, limit,
						new Sort(new SortField("freq", SortField.Type.STRING, true)));
				for (ScoreDoc sd : hits.scoreDocs) {
					String word = isearcher.doc(sd.doc).getField("word").stringValue();
					ret.add(word);
				}
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			ex.printStackTrace();
		} finally {
			BooleanQuery.setMaxClauseCount(mcc);
		}
		return ret;
	}
}
