/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.logging.Logger;

public class Diff {

	public static Logger LOGGER;

	private Properties config1;

	private Properties config2;

	public Diff(String config_name1, String config_name2) throws Exception {
		super();
		LOGGER.config("Config name1: " + config_name1);
		this.config1 = Config.load(config_name1);
		LOGGER.config("Config name2: " + config_name2);
		this.config2 = Config.load(config_name2);
	}

	public HashSet<String> readFile(Properties config, String file_prop)
			throws Exception {
		String data_dir = config.getProperty("data_dir");
		LOGGER.config("data_dir: " + data_dir);
		String data_file = config.getProperty(file_prop);
		LOGGER.config(file_prop + ": " + data_file);
		String replace = config.getProperty("replace");
		LOGGER.config("replace: " + replace);
		String[] repl = replace.split(",");
		HashSet<String> res = new HashSet<String>();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				new FileInputStream(data_dir + data_file), "UTF-8"));
		String line;
		int line_i = 0;
		while ((line = in.readLine()) != null) {
			line_i++;
			if (replace != null)
				line = line.replace(repl[0], repl[1]);
			res.add(line);
		}
		return res;
	}

	public void diff(String file_prop) throws Exception {
		LOGGER.info(file_prop);
		HashSet<String> f1 = readFile(config1, file_prop);
		HashSet<String> f2 = readFile(config2, file_prop);
		LOGGER.info("1-2");
		diff(f1, f2, "1-2");
		LOGGER.info("2-1");
		diff(f2, f1, "2-1");
	}

	public void diff(HashSet<String> f1, HashSet<String> f2, String tag) {
		for (String s1 : f1) {
			if (!(f2.contains(s1)))
				LOGGER.warning(s1 + "\t" + tag);
		}
	}

	public void run() throws Exception {
		for (Object prop_o : Collections.list(config1.propertyNames())) {
			String prop = (String) prop_o;
			if (prop.endsWith("file") && config2.getProperty(prop) != null)
				diff(prop);
		}
	}

	public static void main(String[] args) {
		try {
			Diff.LOGGER = BasicLogger.init(Diff.class);
			String config_name1 = "";
			String config_name2 = "";
			if (args.length > 1) {
				config_name1 = args[0];
				config_name2 = args[1];
			}
			Diff m = new Diff(config_name1, config_name2);
			m.run();
		} catch (Exception ex) {
			BasicLogger.log(ex, LOGGER);
		} finally {
			BasicLogger.end(LOGGER);
		}
	}

}
