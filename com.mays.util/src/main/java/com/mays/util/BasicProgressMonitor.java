/**
 * Copyright 2009-2017 Mays Systems LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *       http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mays.util;

/**
 * @author emays
 * 
 */
public interface BasicProgressMonitor {

	/*
	 * Notifies that the main task is beginning.
	 */
	public void beginTask(String name, int total_work);

	/*
	 * Notifies that the work is done; that is, either the main task is
	 * completed or the user canceled it.
	 */
	public void done();

	/*
	 * Sets the task name to the given value.
	 */
	void setTaskName(String name);

	/*
	 * Notifies that a subtask of the main task is beginning.
	 */
	void subTask(String name);

	/*
	 * Notifies that a given number of work unit of the main task has been
	 * completed.
	 */
	void worked(int work);

}
